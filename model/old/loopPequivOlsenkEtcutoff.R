
elt.txt ='PEquivOlsen'
num.thr = 15
nsim = 100 # For CRPS

# cutoffValue = 0.05 # Cut off of the mesh
# 
# k <- 4 # 5+2 intervals
# source(file = 'FitAndCrossValModelTOWeightsv3.R')
# 
# k <- 6 # 5+2 intervals
# source(file = 'FitAndCrossValModelTOWeightsv3.R')
# 
# k <- 8 # 5+2 intervals
# source(file = 'FitAndCrossValModelTOWeightsv3.R')
# 
# k <- 12 # 5+2 intervals
# source(file = 'FitAndCrossValModelTOWeightsv3.R')

library(foreach)




cutoffValueList = c(0.05,0.08,0.1,0.15,.3)
KList = c(3,8,15)

type = c("XvalCla","XvalST","XvalSTDegrad")


XvalOnly = TRUE

foreach(cutoffValue = cutoffValueList, 
        .combine = rbind.data.frame )  %:%
  
  foreach( k = KList ,
           .combine = rbind.data.frame ) %do% {
             print(c(cutoffValue,k))
             source(file = 'FitAndCrossValModelTOWeightsv3.R')
             
           }

# Synthèse 

r <- foreach(Ci = cutoffValue, 
             .combine = rbind.data.frame )  %:%
  
  foreach( Ki = KList,
           .combine = rbind.data.frame ) %:% 
  foreach( type1 = type ,
           .combine = rbind.data.frame ) %do% {
             Monfichier = paste0(elt.txt,'BretMTOwMatParC',Ci,'S',nsim,'k',Ki)
             # file=paste0("../results/",elt.txt,"/",type1,Monfichier,'.RData')
             file=paste0("../results/","/",type1,Monfichier,'.RData')
             load(file)
             
             # resu2 <- eval(Data_zone$val,Data_zone$pred)
             
             if(type1 == "XvalCla") {
               resu2 <- foreach(i = 1:length(resuXval),
                                .combine = rbind.data.frame ) %do%
                 resuXval[[i]]
             } else {
               resu2 <- foreach(i = 1:length(resuXval),
                                .combine = rbind.data.frame ) %do%
                 resuXval[[i]]
               
             }
             resuM <-   colMeans(resu2)
             
             cbind.data.frame(type1,Ci,Ki,ME=resuM[1],MAE =resuM[2],r = resuM[3],r2=resuM[4],
                              NSE = resuM[5],rhoC=resuM[6],Cb=resuM[7], CRPS= resuM[8])
           }

rfilt <- r[r$type1=="XvalST",]
choix <- which(rfilt$NSE == max(rfilt$NSE))

Ki = rfilt[choix,"Ki"]

Ci = rfilt[choix,"Ci"] # Cut off of the mesh

Monfichier = paste0(elt.txt,'BretMTOwMatParC',Ci,'S',nsim,'k',Ki)[1]

saveRDS(list(Monfichier , Ki, Ci) , 
        paste0("../results/","/","ModelFinal",elt.txt,'.RData'))

XvalOnly = FALSE
source(file = 'FitAndCrossValModelTOWeightsv3.R')

