
# here I define the meshes for the model

xyMesh=as.matrix(Data_zone[,c("x","y")])##build coordinate matrix
xyMesh<-xyMesh/100000 # for INLa keep X and Y small to avoid any numerical instabilty



max.edge = 0.35

 m11 = inla.nonconvex.hull(xyMesh, convex = 0.2)


mesh = inla.mesh.2d(boundary = m11,
                    loc=xyMesh,
                    max.edge = c(1,8)*max.edge, # au lieu de 5
                    cutoff = cutoffValue, # minimum distince between two nodes
                    offset = c(max.edge, bound.outer),
                    min.angle=21)

# A mesh for the residuals

mesh.st = inla.mesh.2d(boundary = m11,
                       loc=xyMesh,
                       max.edge = c(1,8)*max.edge, # au lieu de 5
                       cutoff = cutoffValue * 1.5, # minimum distince between two nodes
                       offset = c(max.edge, bound.outer),
                       min.angle=21)


# Define the temporal mesh
tknots=1:k/(k+1)-0.5

#mesh.time=inla.mesh.1d(tknots,interval=c(0,1),boundary="dirichlet")
mesh.time=inla.mesh.1d(tknots,interval=c(-0.5,0.5),
                       boundary="free",
                       free.clamped=TRUE)
