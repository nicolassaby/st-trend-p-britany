

vvst <- variogram(elt~1, 
                  Data_zoneSTCal[
                    sample(x = 1:nrow(Data_zoneSTCal@data), 
                           500)],
                  width=20000,
                  cutoff=200000,
                  tlags= 365 * c(0,2,5,10,15,20,30) ,
                  tunit = "days"
                  )


sumMetric <- vgmST("sumMetric", 
                   space = vgm(psill=.1,"Sph", range=150000, nugget=0.05),
                   time = vgm(psill=.1,"Sph", range=150000, nugget=0.01),
                   joint = vgm(psill=1,"Sph", range=15000, nugget=10),
                   stAni=500) 

# lower and upper bounds
pars.l <- c(sill.s = 0, 
            range.s = 10, 
            nugget.s = 0.01,
            sill.t = 0,
            range.t = 1, 
            nugget.t = .01,
            sill.st = 0,
            range.st = 10,
            nugget.st = 0,
            anis = 0)

pars.u <- c(sill.s = 200, 
            range.s = 200000, 
            nugget.s = .5,
            sill.t = 1, 
            range.t = 200000, 
            nugget.t = 100,
            sill.st = 200,
            range.st = 200000, 
            nugget.st = 100,
            anis = 10) 

sumMetric_Vgm <- fit.StVariogram(vvst, 
                                 sumMetric, 
                                 method="L-BFGS-B",
                                 lower=pars.l,
                                 upper=pars.u,
                                 tunit="days",
                                 fit.method = 7)


# plot(vvst, sumMetric)

pred <- krigeST(elt~1, 
                data=Data_zoneSTCal,
                nmax = 1500 ,
                modelList=sumMetric_Vgm, 
                newdata=Valid.ST) 
