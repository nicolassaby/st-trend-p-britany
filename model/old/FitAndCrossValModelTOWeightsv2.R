# 1 Packages-------------------------------------------------------
library(INLA)
library(sf)
library(dplyr)
library(sp)
library(foreach)
library(scoringRules)

inla.setOption("pardiso.license", "~/admin/pardiso.lic")

inla.setOption(enable.inla.argument.weights=TRUE)


MaTransform <- function(x , elt){
  if (elt == "pH")  x <- x
  if (elt == "Carbone") x <- log(x)
  if (elt == "PEquivOlsen") x <- sqrt(x)
  return(x)
}




## Parameters ---------- 

path = '~/pari/st-trend-p-britany/'
pathdata = '~/pari/'

num.thr = 5

k <- 4 # 5+2 intervals

nsim = 100 # For CRPS

# Define the mesh
cutoffValue = 0.05 # Cut off of the mesh
bound.outer = 1.2

elt.txt ='ph'

Monfichier = paste0(elt.txt,'BretMTOw','C',cutoffValue,'S',nsim,'k',k)



load( paste0(pathdata,'data/DataBretagne',elt.txt,'.RData') )

source(paste0(path,'model/basicStats.R') )


# nsub=150000
# Data_zone <- Data_zone[sample(1:nrow(Data_zone) , nsub ),]


table(Data_zone$annee)

set.seed(345)

print(Monfichier)

# Create the folding information for classical Xval  --------------

Data_zone <- Data_zone %>% 
mutate_at(vars(elt.txt), .funs = funs(elt = MaTransform(.,elt.txt))) %>% # transformation
mutate(eltRaw = elt ) %>% # For the transformation, save the observed value
  mutate(val = elt ) %>% # For the validation, save the observed value
  mutate(pred = NA , # pour prediction ST Xval
         pred2 = NA, # pour prediction Classical Xval
         n2 = n /max(n))



#collect all data into a "stack":
##define intercept
Data_zone$intercept <- 1 # to avoid to use the INLA intercept 

 covarinla=Data_zone[,c("intercept","annee","mois","matcl","nlogCl","FIRST_ENZ")]
# covarinla=Data_zone[,c("intercept","annee","matcl","nlogCl","FIRST_ENZ","PolsenFinalFlag")]

# Convert to integer for INLA
covarinla$matcl = as.integer(covarinla$matcl)
covarinla$nlogCl = as.integer(covarinla$nlogCl)
covarinla$FIRST_ENZ = as.integer(covarinla$FIRST_ENZ)
#covarinla$PolsenFinalFlag = as.integer(covarinla$PolsenFinalFlag)


# Fit the models TO -------------
OptionVerbose  = TRUE
OptionComputeSimul  = TRUE

source(paste0(path,'model/modelTOWeights',elt.txt,'.R'))

summary(Myfit)

save(mesh,mesh.st ,Myfit,mystack,
     file=paste0(path,'results/',"Fit",Monfichier,'.RData') )

# Perform Simulations  -------------------
samples=inla.posterior.sample(nsim,Myfit)

names(samples[[1]])
length(samples[[1]]$latent)
head(samples[[1]]$latent)


name.effects=c("intercept","matcl","FIRST_ENZ", "space", "time", "spacetime")

#dans cette liste, on mettra les simulations des différentes composantes du prédicteur
sim.components=vector("list",length=length(name.effects))
names(sim.components) <- name.effects

for(i in 1:length(name.effects)){
  
  nch=nchar(name.effects[i])
  
  if (i !=1) {
    idx=which(substr(rownames(samples[[1]]$latent),1,nch+1)==paste0(name.effects[i],":"))
  } else {
    idx=which(substr(rownames(samples[[1]]$latent),1,nch)==name.effects[i])
  }
  length(idx)
  for(j in 1:nsim){
    sim.components[[i]]=cbind(sim.components[[i]],samples[[j]]$latent[idx,])
  }
}

dim(sim.components[[2]]) # les effets spatiaux simulés
dim(sim.components[[3]]) # les effets spatiaux simulés
dim(sim.components[[4]]) # les effets spatiaux simulés

save(sim.components, file=paste0(path,'results/',"Simul",Monfichier,'.RData'))


# Fit the models Degraded  -------------
OptionVerbose  = FALSE
OptionComputeSimul  = FALSE

source(paste0(path,'model/modelTOdegrade',elt.txt,'.R') )
# 
tmp = summary(Myfit)
save(tmp, file=paste0(path,'results/',"summaryModelTODegraded",Monfichier,'.RData'))
# 



# Cross ST  validation  -------------------
gc()

#setwd("C:/Users/nsaby/SharePoint/Pari Scientifique - Documents 1/scripts/crossvalidation/")

resuXval2 <- 
  foreach(i = unique(Data_zone$fold),
          .errorhandling='pass' ) %do% {
            
            print(i)
            
            # Mettre en NA les individus pour la validation crois?e
            Data_zone$elt <- Data_zone$val
            Data_zone$elt[Data_zone$fold==i] <- NA
            
            
            OptionVerbose  = FALSE
            OptionComputeSimul  = TRUE
            source(paste0(path,'model/modelTOWeights',elt.txt,'.R') )
            
            fitted <- Myfit$summary.fitted.values$mean[1:length(mystack$data$data$y)]
            
            mask <- is.na(mystack$data$data$y)
            
            resu <- eval(fitted[mask],
                                 Data_zone$val[Data_zone$fold==i] )
            
            
            Data_zone$pred[Data_zone$fold==i] <- fitted[mask]
            
            idx = which(Data_zone$fold==i) # mettre ici l'indice des observations utilisées avec inla qu'on cherche à prédire 
            # ATTENTION la ligne suivante peut prendre beaucoup de ressources : 
            sample = inla.posterior.sample(nsim, Myfit) 
            # length(sample)
            sims = list()
            for(s in 1:nsim){
              sims[[s]] = sample[[s]]$latent[idx]+rnorm(length(idx), sd = 1/sqrt(Myfit$summary.hyperpar[1,1]))
            }
            
            
            predmat = matrix(unlist(sims), byrow = FALSE, ncol = nsim)
            crps_values = crps_sample(Data_zone$val[Data_zone$fold==i], predmat) # put observed values "observed" here
            
            
            c(resu,CRPS=mean(crps_values) )
          }

save( resuXval2 , Data_zone , file=paste0(path,'results/',"XvalST",Monfichier,'.RData') )


# Cross Classical  validation  -------------------
gc()




resuXvalClassique <- 
  foreach(i = unique(Data_zone$fold2),.errorhandling='pass' ) %do% {
    
    print(i)
    
    Data_zone$elt <- Data_zone$val
    Data_zone$elt[Data_zone$fold2==i] <- NA
    
    
    OptionVerbose  = FALSE
    OptionComputeSimul  = TRUE
    source(paste0(path,'model/modelTOWeights',elt.txt,'.R') )
    
    gc()
    
    fitted <- Myfit$summary.fitted.values$mean[1:length(mystack$data$data$y)]
    
    mask <- is.na(mystack$data$data$y)
    
    resu <- eval(fitted[mask],
                         Data_zone$val[Data_zone$fold2==i] )
    
    
    Data_zone$pred2[Data_zone$fold2==i] <- fitted[mask]
    
    idx = which(Data_zone$fold2==i) # mettre ici l'indice des observations utilisées avec inla qu'on cherche à prédire 
    
    # ATTENTION la ligne suivante peut prendre beaucoup de ressources : 
    sample = inla.posterior.sample(nsim, Myfit) 
    # length(sample)
    sims = list()
    for(s in 1:nsim){
      sims[[s]] = sample[[s]]$latent[idx]+rnorm(length(idx), sd = 1/sqrt(Myfit$summary.hyperpar[1,1]))
    }
    
    
    predmat = matrix(unlist(sims), byrow = FALSE, ncol = nsim)
    crps_values = crps_sample(Data_zone$val[Data_zone$fold2==i], predmat) # put observed values "observed" here
    
    
    c(resu,CRPS=mean(crps_values) )
    
  }

save( resuXvalClassique , Data_zone , file=paste0(path,'results/',"XvalCla",Monfichier,'.RData') )


# Cross ST  validation  degrade -------------------
gc()


resuXval2 <- 
  foreach(i = unique(Data_zone$fold),
          .errorhandling='pass' ) %do% {
            
            print(i)
            
            # Mettre en NA les individus pour la validation crois?e
            Data_zone$elt <- Data_zone$val
            Data_zone$elt[Data_zone$fold==i] <- NA
            
            
            OptionVerbose  = FALSE
            OptionComputeSimul  = TRUE
            source(paste0(path,'model/modelTOdegrade',elt.txt,'.R'))
            
            fitted <- Myfit$summary.fitted.values$mean[1:length(mystack$data$data$y)]
            
            mask <- is.na(mystack$data$data$y)
            
            resu <- eval(fitted[mask],
                                 Data_zone$val[Data_zone$fold==i] )
            
            
            Data_zone$pred[Data_zone$fold==i] <- fitted[mask]
            
            idx = which(Data_zone$fold==i) # mettre ici l'indice des observations utilisées avec inla qu'on cherche à prédire 
            # ATTENTION la ligne suivante peut prendre beaucoup de ressources : 
            sample = inla.posterior.sample(nsim, Myfit) 
            # length(sample)
            sims = list()
            for(s in 1:nsim){
              sims[[s]] = sample[[s]]$latent[idx]+rnorm(length(idx), sd = 1/sqrt(Myfit$summary.hyperpar[1,1]))
            }
            
            
            predmat = matrix(unlist(sims), byrow = FALSE, ncol = nsim)
            crps_values = crps_sample(Data_zone$val[Data_zone$fold==i], predmat) # put observed values "observed" here
            
            
            c(resu,CRPS=mean(crps_values) )
          }

save( resuXval2 , Data_zone , file=paste0(path,'results/',"XvalST","Degrad",Monfichier,'.RData') )


# Cross ST  validation  degrade2 -------------------
gc()


resuXval2 <- 
  foreach(i = unique(Data_zone$fold),
          .errorhandling='pass' ) %do% {
            
            print(i)
            
            # Mettre en NA les individus pour la validation crois?e
            Data_zone$elt <- Data_zone$val
            Data_zone$elt[Data_zone$fold==i] <- NA
            
            
            OptionVerbose  = FALSE
            OptionComputeSimul  = TRUE
            source(paste0(path,'model/modelTOdegrade2',elt.txt,'.R'))
            
            fitted <- Myfit$summary.fitted.values$mean[1:length(mystack$data$data$y)]
            
            mask <- is.na(mystack$data$data$y)
            
            resu <- eval(fitted[mask],
                                 Data_zone$val[Data_zone$fold==i] )
            
            
            Data_zone$pred[Data_zone$fold==i] <- fitted[mask]
            
            idx = which(Data_zone$fold==i) # mettre ici l'indice des observations utilisées avec inla qu'on cherche à prédire 
            # ATTENTION la ligne suivante peut prendre beaucoup de ressources : 
            sample = inla.posterior.sample(nsim, Myfit) 
            # length(sample)
            sims = list()
            for(s in 1:nsim){
              sims[[s]] = sample[[s]]$latent[idx]+rnorm(length(idx), sd = 1/sqrt(Myfit$summary.hyperpar[1,1]))
            }
            
            
            predmat = matrix(unlist(sims), byrow = FALSE, ncol = nsim)
            crps_values = crps_sample(Data_zone$val[Data_zone$fold==i], predmat) # put observed values "observed" here
            
            
            c(resu,CRPS=mean(crps_values) )
          }

save( resuXval2 , Data_zone , file=paste0(path,'results/',"XvalST","Degrad2",Monfichier,'.RData') )




# Cross Classical  validation  -------------------
# gc()
# 
# 
# resuXvalClassique <- 
#   foreach(i = unique(Data_zone$fold2),.errorhandling='pass' ) %do% {
#     
#     print(i)
#     
#     Data_zone$elt <- Data_zone$val
#     Data_zone$elt[Data_zone$fold2==i] <- NA
#     
#     
#     OptionVerbose  = FALSE
#     OptionComputeSimul  = TRUE
#     
#     source(paste0(path,'model/modelTOdegrade',elt.txt,'.R') )
#     
#     gc()
#     
#     fitted <- Myfit$summary.fitted.values$mean[1:length(mystack$data$data$y)]
#     
#     mask <- is.na(mystack$data$data$y)
#     
#     resu <- summaryStats(fitted[mask],
#                          Data_zone$val[Data_zone$fold2==i] )
#     
#     
#     Data_zone$pred2[Data_zone$fold2==i] <- fitted[mask]
#     
#     idx = which(Data_zone$fold2==i) # mettre ici l'indice des observations utilisées avec inla qu'on cherche à prédire 
#     # ATTENTION la ligne suivante peut prendre beaucoup de ressources : 
#     
#     sample = inla.posterior.sample(nsim, Myfit) 
#     # length(sample)
#     sims = list()
#     for(s in 1:nsim){
#       sims[[s]] = sample[[s]]$latent[idx]+rnorm(length(idx), sd = 1/sqrt(Myfit$summary.hyperpar[1,1]))
#     }
#     
#     
#     predmat = matrix(unlist(sims), byrow = FALSE, ncol = nsim)
#     crps_values = crps_sample(Data_zone$val[Data_zone$fold2==i], predmat) # put observed values "observed" here
#     
#     
#     c(resu,CRPS=mean(crps_values) )
#     
#   }
# 
# save( resuXvalClassique , Data_zone , file=paste0(path,'results/',"XvalCla",'Degrad',Monfichier,'.RData') )
# 

