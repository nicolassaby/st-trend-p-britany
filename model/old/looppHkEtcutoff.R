
elt.txt ='ph'
num.thr = 15
nsim = 100 # For CRPS

# cutoffValue = 0.05 # Cut off of the mesh
# 
# k <- 4 # 5+2 intervals
# source(file = 'FitAndCrossValModelTOWeightsv3.R')
# 
# k <- 6 # 5+2 intervals
# source(file = 'FitAndCrossValModelTOWeightsv3.R')
# 
# k <- 8 # 5+2 intervals
# source(file = 'FitAndCrossValModelTOWeightsv3.R')
# 
# k <- 12 # 5+2 intervals
# source(file = 'FitAndCrossValModelTOWeightsv3.R')

library(foreach)

cutoffValueList = c(0.05,0.08,0.1,0.15,0.3)
KList = c(3,6,8,15)

XvalOnly = TRUE

foreach(cutoffValue = cutoffValueList, 
        .combine = rbind.data.frame )  %:%
  
  foreach( k = KList ,
           .combine = rbind.data.frame ) %do% {
             source(file = 'FitAndCrossValModelTOWeightsv3.R')
             
           }

# Synthèse 

type = c("XvalCla","XvalST","XvalSTDegrad","XvalClaDegrad2")

r <- foreach(Ci = cutoffValueList, 
             .combine = rbind.data.frame,
             .errorhandling = c("remove") )  %:%
  
  foreach( Ki = KList,
           .combine = rbind.data.frame
            ) %:% 
  foreach( type1 = type ,
           .combine = rbind.data.frame ) %do% {
             Monfichier = paste0(elt.txt,'BretMTOwMatParC',Ci,'S',nsim,'k',Ki)
             # file=paste0("../results/",elt.txt,"/",type1,Monfichier,'.RData')
             file=paste0("../results","/",type1,Monfichier,'.RData')
             
             print(file)
             load(file)
             print("ok")
             
             # resu2 <- eval(Data_zone$val,Data_zone$pred)
             
             if(type1 == "XvalCla") {
               resu2 <- foreach(i = 1:length(resuXval),
                                .combine = rbind.data.frame ) %do%
                 resuXval[[i]]
             } else {
               resu2 <- foreach(i = 1:length(resuXval),
                                .combine = rbind.data.frame ) %do%
                 resuXval[[i]]
               
             }
             resuM <-   colMeans(resu2)
             
             cbind.data.frame(type1,Ci,Ki,ME=resuM[1],MAE =resuM[2],r = resuM[3],r2=resuM[4],
                              NSE = resuM[5],rhoC=resuM[6],Cb=resuM[7], CRPS= resuM[8])
           }

rfilt <- r[r$type1=="XvalST",]
choix <- which(rfilt$NSE == max(rfilt$NSE))

k = Ki = rfilt[choix,"Ki"]

cutoffValue = Ci = rfilt[choix,"Ci"] # Cut off of the mesh

Monfichier = paste0(elt.txt,'BretMTOwMatParC',Ci,'S',nsim,'k',Ki)[1]

saveRDS(Monfichier,paste0("../results/","/","ModelFinal",elt.txt,'.RData'))

XvalOnly = FALSE
source(file = 'FitAndCrossValModelTOWeightsv3.R')
