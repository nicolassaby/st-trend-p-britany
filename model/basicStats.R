
MPE <- function(x, y){
  return (mean(x-y))
}	

SDPE <- function(x, y){
return ((sd(x - y)))
}

RMSPE <- function(x, y){
return (mean((x-y)^2)^0.5)
}


MedPE <- function(x, y){
  return (median(x-y))
}	

RMedSPE <- function(x, y){
return (median((x-y)^2)^0.5)
}

RE <- function(x, y){
  return (mean((x-y)/x) )
}

R2 <- function(x, y){
return (cor(x,y)^2)
}

summaryStats <- function(pred, obs){

  
  res <- c(cor(pred, obs)^2, MPE(pred, obs), SDPE(pred, obs), RMSPE(pred, obs),RE(pred, obs))
  names(res) <- c("R2", "MPE", "SDPE", "RMSPE","RE")
  return (res)
#   
}

eval <- function(x, y){
  
  # mean error
  ME <- round(mean(y - x, na.rm = TRUE), digits = 2)
  
  # root mean square error
  RMSE <-   round(sqrt(mean((y - x)^2, na.rm = TRUE)), digits = 2)
  
  # root mean absolute error
  MAE <-   round(mean(abs(y - x), na.rm = TRUE), digits = 2)
  
  # Pearson's correlation squared
  r2 <-  round((cor(x, y, method = 'pearson', use = 'pairwise.complete.obs')^2), digits = 2)
  
  # MEC
  SSE <- sum((y - x) ^ 2, na.rm = T)
  SST <- sum((y - mean(y, na.rm = T)) ^ 2, na.rm = T)
  NSE <- round((1 - SSE/SST), digits = 2)
  
  # concordance correlation coefficient
  n <- length(x)
  sdx <- sd(x, na.rm = T)
  sdy <- sd(y, na.rm = T)
  r <- stats::cor(x, y, method = 'pearson', use = 'pairwise.complete.obs')
  # scale shift
  v <- sdx / sdy
  sx2 <- var(x, na.rm = T) * (n - 1) / n
  sy2 <- var(y, na.rm = T) * (n - 1) / n
  # location shift relative to scale
  u <- (mean(x, na.rm = T) - mean(y, na.rm = T)) / ((sx2 * sy2)^0.25)
  Cb <- ((v + 1 / v + u^2)/2)^-1
  rCb <- r * Cb
  rhoC <- round(rCb, digits = 2)
  
  Cb <- round(Cb, digits = 2)
  r <- round(r, digits = 2)
  
  # return the results
  evalRes <- data.frame(ME = ME, MAE = MAE, RMSE = RMSE, r = r, r2 = r2, NSE = NSE, rhoC = rhoC, Cb = Cb)
  
  return(evalRes)
}

eval <- function(x, y){
  
  # mean error
  ME <- round(mean(y - x, na.rm = TRUE), digits = 2)
  
  # root mean square error
  RMSE <-   round(sqrt(mean((y - x)^2, na.rm = TRUE)), digits = 2)
  
  # root mean absolute error
  MAE <-   round(mean(abs(y - x), na.rm = TRUE), digits = 2)
  
  # Pearson's correlation squared
  r2 <-  round((cor(x, y, method = 'pearson', use = 'pairwise.complete.obs')^2), digits = 2)
  
  # MEC
  SSE <- sum((y - x) ^ 2, na.rm = T)
  SST <- sum((y - mean(y, na.rm = T)) ^ 2, na.rm = T)
  NSE <- round((1 - SSE/SST), digits = 2)
  
  # concordance correlation coefficient
  n <- length(x)
  sdx <- sd(x, na.rm = T)
  sdy <- sd(y, na.rm = T)
  r <- stats::cor(x, y, method = 'pearson', use = 'pairwise.complete.obs')
  # scale shift
  v <- sdx / sdy
  sx2 <- var(x, na.rm = T) * (n - 1) / n
  sy2 <- var(y, na.rm = T) * (n - 1) / n
  # location shift relative to scale
  u <- (mean(x, na.rm = T) - mean(y, na.rm = T)) / ((sx2 * sy2)^0.25)
  Cb <- ((v + 1 / v + u^2)/2)^-1
  rCb <- r * Cb
  rhoC <- round(rCb, digits = 2)
  
  Cb <- round(Cb, digits = 2)
  r <- round(r, digits = 2)
  
  # return the results
  evalRes <- data.frame(ME = ME, MAE = MAE, RMSE = RMSE, r = r, r2 = r2, NSE = NSE, rhoC = rhoC, Cb = Cb)
  
  return(evalRes)
}


jeffrey <- function(X, a1, a2) {
    C <- X	
    expr <- expression(a1 + a2 * log10(C))
    eval(expr)
}


federer <- function(X, a1, a2, a3) {
    C <- X	
    expr <- expression(a1 + a2 * log(C) + a3 * log(C)^2)
    eval(expr)
}

adams <- function(X, a1, a2) {
    C <- X	
    expr <- expression(100/(a1 + a2 * C))
    eval(expr)
}

manrique <- function(X, a1, a2) {
    C <- X	
    expr <- expression(a1 + a2 * C^0.5)
    eval(expr)
}

kaur <- function(X, a1, a2, a3, a4, a5) {
    C <- X[,1]
    a <- X[,2]
    lim <- X[,3] + X[,4]
    expr <- expression(a1 + a2 * C + a3 * a + a4 * a^2 + a5 * lim)
    eval(expr)
}

# cost function
fcn <- function(p, X, N, N.Err, fcall)
#    (N - do.call(fcall, c(list(X = X), as.list(p))))/N.Err
    (N - do.call(fcall, c(list(X = X), as.list(p))))^2
#    (N - do.call(fcall, c(list(X = X), as.list(p))))

# function to ensure the compatibility of the soil codes
code <- function(soilType){
	if (is.na(soilType) || soilType == '') return (NA)
	soils <- c( 'alocrisol',
	 'anthroposol', 'arenosol',
	 'brunisol',
	 'calcisol', 'calcosol', 'colluviosol',
	 'fersialsol', 'fluviosol',
	 'luvisol',
	 'magnesisol',
	 'neoluvisol',
	 'pelosol',
	 'planosol',
	 'podzosol',
	 'rankosol', 'redoxisol', 'reductisol', 'rendisol', 'rendosol',
	 'salisol',
	 'thalassosol',
	 'vertisol',
	)
	res <-  (1:length(soils))[soils == soilType]	
	if (length(res) == 0)
		warning(paste("soil type ", soilType,
				" not found when generating a code for MART"))
	return (res)
}


# fits the mart model with default parameters
fitMart <- function(name, lxe, vNames, data, resp, nitera){
train <- data[,vNames]
if ("soil" %in% vNames)
	train$soil <- as.numeric(unclass(train$soil))
x <- apply(train[,vNames[-1]], 2, as.numeric)
y <- train[,resp]
mart(as.matrix(x),y,lxe,niter=nitera, tree.size=5,learn.rate=0.05, loss.cri=2)
martsave(name)
}

# fits the mart model with default parameters
fitGbm <- function(vNames, data, resp, ts, bf, nt, minobs = 10, cv, lr = 0.05){
train <- data[,vNames]
y <- train[,resp]
formula <- paste(resp, " ~ ", paste(vNames[2:length(vNames)], collapse=" + "))
#monotonic <- rep(0,length(vNames))
m <- gbm(
    as.formula(formula),         # formula
    data = train,                   # dataset
#    var.monotone=monotonic,                           
    distribution="gaussian",
    n.trees=nt,           
    shrinkage = lr,                                
    interaction.depth=ts,    
    bag.fraction = bf,       
    train.fraction = 1,     
    n.minobsinnode = minobs,  
    cv.folds = cv,         
    keep.data=TRUE,       
    verbose=TRUE)
return (m)
}

# fits the mart model with default parameters
fitMartNoInteraction <- function(name, lxe, vNames, data){
train <- data[,vNames]
if ("soil" %in% vNames)
	train$soil <- as.numeric(unclass(train$soil))
x <- apply(train[,vNames[-1]], 2, as.numeric)
y <- train$dapp
mart(as.matrix(x),y,lxe,niter=6000, tree.size=5,learn.rate=0.05, loss.cri=2)
martsave(name)
}

# trying to do a more generic function
#
fit <- function(train, p, ptfName, method){
   if (method == "levmar"){
	X <- train[,-1]
	Y <- train[,1]	
	mod <- nls.lm(par = p, fn = fcn, fcall = ptfName,
	              X = X, N = Y, N.Err = sqrt(Y),
              control = list(ftol=1e-10)
)
   }		
   else if (method == "gaunew"){
	   mod <- nls(formulasNlm[ptfName], train, start = p,
			   control=list(minFactor=1/4026))
   }
   else {
	   mod <- lm(as.formula(formulasLm[ptfName]), train)   
   }
}

# comparaison des methodes d'ajustement
# nb the train data must have a X as well as Y columns
predictModel <- function(model, train, ptfName, method){
   if (method == "levmar"){
	X <- train[,-1]
	Y <- train[,1]	
	Ypred <- do.call(ptfName, c(list(X = X), model$par))
   }		
   else {
	Ypred <- predict(model, newdata = train)
   }
   return (Ypred)
}

# makes a bootstrap or crossvalidation (?) for validationg a model
# by splitting the dataset into training and validation sample
crossValidateMart <- function(trainx, trainy, lx, probs, nbl, nits, ts, lr){
#    browser()
for (prob in probs){
	
  #prob <- 0.1+i/10
  nbTrue <- min(dim(trainx)[1], round(dim(trainx)[1] * prob))
  masko <- c(rep(T, nbTrue), rep(F, dim(trainx)[1] - nbTrue)) 

  for (j in 1:nbl){	
#    brower()
    masko <- sample(masko)
    
    x <- trainx[masko,]
    y <- trainy[masko]
#    mart(as.matrix(x),y,lx,niter=3000, tree.size=5,learn.rate=0.05, loss.cri=2)
    if (nits > 10000) {    
	mart(as.matrix(x),y,lx,niter=10000, tree.size=ts,learn.rate=lr, loss.cri=2)
	loops <- 0	
        while (loops < (nits - 10000)){
		moremart()
		loops <- loops + 200	
	}
    }
    else 
	mart(as.matrix(x),y,lx,niter=nits, tree.size=ts,learn.rate=lr, loss.cri=2)   
    
    f.predict <- martpred(as.matrix(x))
    if (!exists("resext"))
	resin <- c(cor(y, f.predict)^2,	MPE(y, f.predict),
    		SDPE(y, f.predict), RMSPE(y, f.predict), prob)	    
    else 
	resin <- rbind(resin, c(cor(y, f.predict)^2,	MPE(y, f.predict),
    		SDPE(y, f.predict), RMSPE(y, f.predict), prob))
    
    y <- trainy[!masko]
    f.predict <- martpred(as.matrix(trainx[!masko,]))
    
    if (!exists("resext"))
	resext <- c(cor(y, f.predict)^2,	MPE(y, f.predict),
    		SDPE(y, f.predict), RMSPE(y, f.predict), prob)
    else 
	resext <- rbind(resext, c(cor(y, f.predict)^2,	MPE(y, f.predict),
    		SDPE(y, f.predict), RMSPE(y, f.predict), prob))

   print(paste("loop ", j, " , prob ", prob, " done."))
    
  }
}

res <- data.frame(resin, resext)
names(res) <- c("r2", "MPE", "SDPE", "RMSPE", "prob", "ext.r2", "ext.MPE",
		"ext.SDPE", "ext.RMSPE", "ext.prob")
return(res)
}

crossValidateGbm <- function(data, vNames, probs, nbl, nits, ts, lr, cv, nobs = 10, bf = 1, f1, f2){
 for (prob in probs){

  datax <- data[, vNames[-1]]
  datay <- data[, vNames[1]]

  nbTrue <- min(dim(datax)[1], round(dim(datax)[1] * prob))
  masko <- c(rep(T, nbTrue), rep(F, dim(datax)[1] - nbTrue)) 

  for (j in 1:nbl){	
    masko <- sample(masko)
    
    x <- datax[masko,]
    y <- datay[masko]
    
    formula <- as.formula(paste(vNames[1], " ~ ", paste(vNames[-1],
				    collapse=" + ")))
    print(formula)
    m <- gbm(formula, data = data[masko,], distribution="gaussian", n.trees=nits,           
    shrinkage=lr, interaction.depth= ts,
    bag.fraction = bf, train.fraction = 1, n.minobsinnode = nobs, cv.folds = cv,         
    keep.data=TRUE, verbose=F)

    best.iter = gbm.perf(m, method = "cv",plot.it = F)
#    best.iter <- trunc(nits*3/4)
#    best.iter <- nits	
    f.predict <- predict.gbm(m, x, best.iter)
    

    if (!exists("resext"))
	resin <- c(cor(y, f.predict)^2,	MPE(y, f.predict),
    		SDPE(y, f.predict), RMSPE(y, f.predict), best.iter, prob)	    
    else 
	resin <- rbind(resin, c(cor(y, f.predict)^2,	MPE(y, f.predict),
    		SDPE(y, f.predict), RMSPE(y, f.predict), best.iter, prob))
    
    indepy <- datay[!masko]
    indep.pred <-  predict.gbm(m, datax[!masko,], best.iter)
    
    if (!exists("resext"))
	resext <- c(cor(indepy, indep.pred)^2,	MPE(indepy, indep.pred),
    		SDPE(indepy, indep.pred), RMSPE(indepy, indep.pred), prob)
    else 
	resext <- rbind(resext, c(cor(indepy, indep.pred)^2,	MPE(indepy, indep.pred),
    		SDPE(indepy, indep.pred), RMSPE(indepy, indep.pred), prob))
     
   print(paste("best.iter=", best.iter, "loop ", j, " , prob ", prob, " done."))
#browser()
    
   }
 }

 res <- data.frame(resin, resext)
 names(res) <- c("r2", "MPE", "SDPE", "RMSPE", "best.iter", "prob", "ext.r2",
		"ext.MPE", "ext.SDPE", "ext.RMSPE", "ext.prob")
 return(res)
}



crossValidateGbmDetUncertainties <-
	function(data, vNames, clayName, probs, nbl, nits, ts, lr, cv, nobs = 10, bf = 1, f1, f2){
 for (prob in probs){

  datax <- data[, vNames[-1]]
  datay <- data[, vNames[1]]

  nbTrue <- min(dim(datax)[1], round(dim(datax)[1] * prob))
  masko <- c(rep(T, nbTrue), rep(F, dim(datax)[1] - nbTrue)) 

  for (j in 1:nbl){	
    masko <- sample(masko)
    
    x <- datax[masko,]
    y <- datay[masko]
    
    formula <- as.formula(paste(vNames[1], " ~ ", paste(vNames[-1],
				    collapse=" + ")))
    print(formula)
    m <- gbm(formula, data = data[masko,], distribution="gaussian", n.trees=nits,           
    shrinkage=lr, interaction.depth= ts,
    bag.fraction = bf, train.fraction = 1, n.minobsinnode = nobs, cv.folds = cv,         
    keep.data=TRUE, verbose=F)

    best.iter = gbm.perf(m, method = "cv",plot.it = F)
#    best.iter <- trunc(nits*3/4)
#    best.iter <- nits	
    f.predict <- predict.gbm(m, x, best.iter)
    
    
    if (!exists("resext"))
	resin <- c(cor(y, f.predict)^2,	MPE(y, f.predict),
    		SDPE(y, f.predict), RMSPE(y, f.predict), best.iter, prob)	    
    else 
	resin <- rbind(resin, c(cor(y, f.predict)^2,	MPE(y, f.predict),
    		SDPE(y, f.predict), RMSPE(y, f.predict), best.iter, prob))
    
    indepy <- datay[!masko]
    indep.pred <-  predict.gbm(m, datax[!masko,], best.iter)
#browser()
    temp <- (data.frame(cbind(indep.pred,C = indepy)))
    f1 <- (trunc(datax[!masko, clayName]/50) * 50 + 25)
    f2 <- (trunc(indep.pred))
    temp$f1 <- as.factor(f1)
    temp$f2 <- as.factor(f2)

    valsClay <- by(temp[,1:2], list(f1 = f1), function(x) RMSPE(x[,1], x[,2]))
    valsC <- by(temp[,1:2], list(f2 = f2), function(x) RMSPE(x[,1], x[,2]))

    fclay <- as.factor(trunc(1:1000/50) * 50 + 25)
    fc <- as.factor(trunc(0:27))
    resUnClay <- rep(NA,21)
    resUnC <- rep(NA,28)
    names(resUnClay) <- levels(fclay)
    names(resUnC) <- levels(fc)
    
    resUnC[names(valsC)] <- valsC
    resUnClay[names(valsClay)] <- valsClay


    if (!exists("resext"))
	resext <- c(cor(indepy, indep.pred)^2,	MPE(indepy, indep.pred),
    		SDPE(indepy, indep.pred), RMSPE(indepy, indep.pred), prob, resUnClay[1:21],
		resUnC[1:28])
    else 
	resext <- rbind(resext, c(cor(indepy, indep.pred)^2,	MPE(indepy, indep.pred),
    		SDPE(indepy, indep.pred), RMSPE(indepy, indep.pred), prob, resUnClay[1:21],
		resUnC[1:28]))
     
   print(paste("best.iter=", best.iter, "loop ", j, " , prob ", prob, " done.", names(fc)))
#browser()
    
   }
 }

 res <- data.frame(resin, resext)
 names(res) <- c("r2", "MPE", "SDPE", "RMSPE", "best.iter", "prob", 
		 "ext.r2", "ext.MPE", "ext.SDPE", "ext.RMSPE", "ext.prob",
		paste("ext",names(resUnClay), sep="."),
		paste("extC",names(resUnC), sep="."))
#browser()

 return(res)
}


crossValidateGbmPlusLm <- function(data, vNames, probs, nbl, nits, ts, lr, cv, nobs = 10, bf = 1){
 for (prob in probs){

  datax <- data[, vNames[-1]]
  datay <- data[, vNames[1]]

  nbTrue <- min(dim(datax)[1], round(dim(datax)[1] * prob))
  masko <- c(rep(T, nbTrue), rep(F, dim(datax)[1] - nbTrue)) 

  for (j in 1:nbl){	
    masko <- sample(masko)
    
    x <- datax[masko,]
    y <- datay[masko]
    
    formula <- as.formula(paste(vNames[1], " ~ ", paste(vNames[-1],
				    collapse=" + ")))
    print(formula)
    m <- gbm(formula, data = data[masko,], distribution="gaussian", n.trees=nits,           
    shrinkage=lr, interaction.depth= ts,
    bag.fraction = bf, train.fraction = 1, n.minobsinnode = nobs, cv.folds = cv,         
    keep.data=TRUE, verbose=F)

    best.iter = gbm.perf(m, method = "cv",plot.it = F)
#    best.iter <- trunc(nits*3/4)
#    best.iter <- nits	
    f.predict <- predict.gbm(m, x, best.iter)
    reg <- lm(f.predict ~ y)
    cs <- reg$coefficients
    f.predict2 <- (f.predict - cs[1]) / cs[2]	

    if (!exists("resext"))
	resin <- c(cor(y, f.predict)^2,	MPE(y, f.predict),
    		SDPE(y, f.predict), RMSPE(y, f.predict),
		cor(y, f.predict2)^2,	MPE(y, f.predict2),
    		SDPE(y, f.predict2), RMSPE(y, f.predict2), best.iter,  prob)	    
    else 
	resin <- rbind(resin, c(cor(y, f.predict)^2,	MPE(y, f.predict),
    		SDPE(y, f.predict), RMSPE(y, f.predict),
		cor(y, f.predict2)^2,	MPE(y, f.predict2),
    		SDPE(y, f.predict2), RMSPE(y, f.predict2), best.iter,  prob))
    
    indepy <- datay[!masko]
    indep.pred <-  predict.gbm(m, datax[!masko,], best.iter)
    indep.pred2 <- (indep.pred - cs[1]) / cs[2]	
    
    if (!exists("resext"))
	resext <- c(cor(indepy, indep.pred)^2,	MPE(indepy, indep.pred),
    		SDPE(indepy, indep.pred), RMSPE(indepy, indep.pred),
		cor(indepy, indep.pred2)^2,	MPE(indepy, indep.pred2),
    		SDPE(indepy, indep.pred2), RMSPE(indepy, indep.pred2),
		prob)
    else 
	resext <- rbind(resext, c(cor(indepy, indep.pred)^2,	MPE(indepy, indep.pred),
    		SDPE(indepy, indep.pred), RMSPE(indepy, indep.pred),
		cor(indepy, indep.pred2)^2,	MPE(indepy, indep.pred2),
    		SDPE(indepy, indep.pred2), RMSPE(indepy, indep.pred2),
		prob))
     
   print(paste("best.iter=", best.iter, "loop ", j, " , prob ", prob, " done."))
#browser()
    
   }
 }

 res <- data.frame(resin, resext)
 names(res) <- c("r2", "MPE", "SDPE", "RMSPE","r22", "MPE2", "SDPE2", "RMSPE2",
		 "best.iter", "prob", "ext.r2",
		"ext.MPE", "ext.SDPE", "ext.RMSPE","ext.r22",
		"ext.MPE2", "ext.SDPE2", "ext.RMSPE2", "ext.prob")
 return(res)
}



# resp : reponse du modele
# probs : fraction des donnees utilis?es pour l'ajustement
# nbl : nombre de r?p?titions
crossValidateLm <- function(data, formula, resp, probs, nbl){
for (prob in probs){

  nbTrue <- min(dim(data)[1], round(dim(data)[1] * prob))
  masko <- c(rep(T, nbTrue), rep(F, dim(data)[1] - nbTrue)) 

  for (j in 1:nbl){	
    masko <- sample(masko)
    train <- data[masko,]
	    
    m <- lm(formula, train)
    y.pred <- predict.lm(m, train)
    
    y <- data[masko, resp]
    best.iter = 0
    
    if (!exists("resext"))
	resin <- c(cor(y, y.pred)^2,	MPE(y, y.pred),
    		SDPE(y, y.pred), RMSPE(y, y.pred), best.iter, prob)	    
    else 
	resin <- rbind(resin, c(cor(y, y.pred)^2,	MPE(y, y.pred),
    		SDPE(y, y.pred), RMSPE(y, y.pred), best.iter, prob))
    
    indep <- data[!masko,]		
    indepy <- indep[, resp]
    indep.pred <- predict.lm(m, indep)
    
    if (!exists("resext"))
	resext <- c(cor(indepy, indep.pred)^2,	MPE(indepy, indep.pred),
    		SDPE(indepy, indep.pred), RMSPE(indepy, indep.pred), prob)
    else 
	resext <- rbind(resext, c(cor(indepy, indep.pred)^2,	MPE(indepy, indep.pred),
    		SDPE(indepy, indep.pred), RMSPE(indepy, indep.pred), prob))
     
#   print(paste("best.iter=", best.iter, "loop ", j, " , prob ", prob, " done."))
#browser()
    
  }
}

res <- data.frame(resin, resext)
names(res) <- c("r2", "MPE", "SDPE", "RMSPE", "best.iter", "prob", "ext.r2",
		"ext.MPE", "ext.SDPE", "ext.RMSPE", "ext.prob")
return(res)
}


# makes a bootstrap or crossvalidation (?) for validationg a model
# by splitting the dataset into training and validation sample
crossValidatePtf <- function(data, nbl, p, ptfName, meth){
nbSamples <- dim(data)[1]
print(meth)	
#if (ptfName == "kaur") {
#	data$lim <- data$lf + data$lg
#        data <- data[,-c(4, 5)]
#}

for (i in 0:8)	{
	
  prob <- 0.1+i/10
  nbTrue <- min(nbSamples, round(nbSamples * prob))
  masko <- c(rep(T, nbTrue), rep(F, nbSamples - nbTrue))

  for (j in 1:nbl){	

    masko <- sample(masko)
    
    XY <- data[masko,]
    Y <- XY[,1]
    # for the kaur fitting
    outM <- fit(XY, p, ptfName, meth)
	
    f.predict <- predictModel(outM, XY, ptfName, meth)
    
    if (!exists("resext"))
	resin <- c(cor(Y, f.predict)^2,	MPE(Y, f.predict),
    		SDPE(Y, f.predict), RMSPE(Y, f.predict), prob)	    
    else 
	resin <- rbind(resin, c(cor(Y, f.predict)^2,	MPE(Y, f.predict),
    		SDPE(Y, f.predict), RMSPE(Y, f.predict), prob))
    
    XY <- data[!masko,]
    Y <-XY[,1]
    f.predict <- predictModel(outM, XY, ptfName, meth)
        
    if (!exists("resext"))
	resext <- c(cor(Y, f.predict)^2,	MPE(Y, f.predict),
    		SDPE(Y, f.predict), RMSPE(Y, f.predict), prob)
    else 
	resext <- rbind(resext, c(cor(Y, f.predict)^2,	MPE(Y, f.predict),
    		SDPE(Y, f.predict), RMSPE(Y, f.predict), prob))

   print(paste("loop ", j, " , prob ", prob, " done."))
    
  }
}

res <- data.frame(resin, resext)
names(res) <- c("r2", "MPE", "SDPE", "RMSPE", "prob", "ext.r2", "ext.MPE",
		"ext.SDPE", "ext.RMSPE", "ext.prob")
return(res)
}


# bootstrap functions
boot.ptf <- function(data, indices, p, ptfName, meth){
	
	XY <- data[indices,]
	Y <- XY[,1]
	outM <- fit(XY, p, ptfName, meth)
	
        f.predict <- predictModel(outM, XY, ptfName, meth)
	
	c(outM$par, cor(Y, f.predict)^2, MPE(Y, f.predict),
	    SDPE(Y, f.predict), RMSPE(Y, f.predict))
}

boot.mart <-   function(data, indices){

        train <- data[indices,]# select obs in the sample
	train$soil <- as.numeric(unclass(train$soil))

	x <- apply(train[,varNames[-1]], 2, as.numeric)
	y <- train$dapp

	mart(as.matrix(x),y,lx,niter=9000, tree.size=5,learn.rate=0.05, loss.cri=2)

	f.predict <- martpred(as.matrix(x))

	c(cor(y, f.predict)^2, MPE(y, f.predict),
    		SDPE(y, f.predict), RMSPE(y, f.predict))
}	

boot.mart.light <-   function(data, indices){

        train <- data[indices,]# select obs in the sample
	train$solrp <- as.numeric(unclass(train$solrp))

	x <- apply(train[,varNamesLight[-1]], 2, as.numeric)
	y <- train$dapp

	mart(as.matrix(x),y,lx,niter=9000, tree.size=5,learn.rate=0.05, loss.cri=2)

	f.predict <- martpred(as.matrix(x))

	c(cor(y, f.predict)^2, MPE(y, f.predict),
    		SDPE(y, f.predict), RMSPE(y, f.predict))
}	

boot.mart.par <- function(data, indices){

        lx <- c(2, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1)

	setwd("c:")
	mart.home <- "\\MART\\MART_win"
	source("\\MART\\MART_win\\mart.s")

        train <- data
	train$solrp <- as.numeric(unclass(train$solrp))

	train <- data[indices,]# select obs in the sample
	x <- apply(train[,varNames[-1]], 2, as.numeric)
	y <- train$dapp

	mart(as.matrix(x),y,lx,niter=3000, tree.size=5,learn.rate=0.05, loss.cri=2)

	train <- data# select obs in the sample
	x <- apply(train[,varNames[-1]], 2, as.numeric)
	y <- train$dapp

	f.predict <- martpred(as.matrix(x))

#	unIndic <<- (indices)
	
	c(cor(y, f.predict)^2, MPE(y, f.predict),
    		SDPE(y, f.predict), RMSPE(y, f.predict))
}


boot.mart.semipar <- function(data, indices){
# Resc and Yb global variables
        Rescb <- Res[indices]
	
	Yb <- Ypred + Rescb

	train <- data[indices,]# select obs in the sample
	x <- apply(train[,varNames[-1]], 2, as.numeric)

	lx <- c(2, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1)
	mart(as.matrix(x),Yb,lx,niter=3000, tree.size=5,learn.rate=0.05, loss.cri=2)

	Ybpred <- martpred(as.matrix(x))

	c(cor(Y, Ybpred)^2, MPE(y, Ybpred), SDPE(y, Ybpred), RMSPE(y, Ybpred))
}

boot.gbm <-   function(data, indices, vNames, nits, ts, lr, cv, nobs, bf){

        train <- data[indices, vNames]# select obs in the sample

	x <- train[, -1]
	y <- train[, 1]
 formula <- as.formula(paste(vNames[1], " ~ ", paste(vNames[-1],
				    collapse=" + ")))

        m <- gbm(formula, data = train, distribution="gaussian", n.trees=nits,           
	  shrinkage= lr, interaction.depth= ts,
	  bag.fraction = bf, train.fraction = 1, n.minobsinnode = nobs,
	  cv.folds = cv,
	  keep.data=TRUE, verbose=F)
	best.iter = gbm.perf(m, method = "cv",plot.it = F)
	f.predict <- predict.gbm(m, train, best.iter)
    
	c(cor(y, f.predict)^2, MPE(y, f.predict),
    		SDPE(y, f.predict), RMSPE(y, f.predict))
}	


plotCrossValidRes <- function(res){
boxplot(r2 ~ prob, data = res)#, ylab="R2",
#		xlab="prop used for fitting", main="apprentissage, r2")
boxplot(MPE ~ prob, data = res)#, ylab="R2",
#		xlab="prop used for fitting", main="apprentisssage, MPE")
boxplot(ext.r2 ~ prob, data = res)#, ylab="R2")#,
#		xlab="prop used for fitting", main="validation, r2")
boxplot(ext.MPE ~ prob, data = res)#, ylab="R2",
#		xlab="prop used for fitting", main="validation, MPE")
}

plotCrossValidRes <- function(res){
  boxplot(SDPE ~ prob, data = res)
  lineNinetyFivePercentIntervals(res$prob, res$SDPE)
  boxplot(ext.SDPE ~ prob, data = res)
  lineNinetyFivePercentIntervals(res$prob, res$ext.SDPE)
  boxplot(RMSPE ~ prob, data = res)
  lineNinetyFivePercentIntervals(res$prob, res$RMSPE)
  boxplot(ext.RMSPE ~ prob, data = res)
  lineNinetyFivePercentIntervals(res$prob, res$ext.RMSPE)
}

plotCrossValidResMedCI2 <- function(res){
  plotMedCI(res$prob, res$SDPE)
  plotMedCI(res$prob, res$ext.SDPE)
  plotMedCI(res$prob, res$RMSPE)
  plotMedCI(res$prob, res$ext.RMSPE)
}

plotCrossValidResMedCI <- function(res, titl){
  t1 <- if (titl) "internal" else ""
  t2 <- if (titl) "external" else ""
  plotMedCI(res$prob, res$r2, t1, c(min(res$r2), max(res$r2)))
  plotMedCI(res$prob, res$ext.r2, t2, c(min(res$r2), max(res$r2)))
#  plotMedCI(res$prob, res$MPE)
#  plotMedCI(res$prob, res$ext.MPE)
}


plotCrossValidResMedCIOverlay <- function(res1, res2, titl){
  t1 <- if (titl) "internal" else ""
  t2 <- if (titl) "external" else ""
#  plotMedCI(res1$prob, res1$r2, t1, c(min(res2$r2), max(res1$r2)))
#  plotMedCI(res2$prob, res2$r2, t1, c(min(res2$r2), max(res1$r2)), redraw = T)
#  plotMedCI(res1$prob, res1$ext.r2, t2, c(min(res2$r2), max(res1$r2)))
#  plotMedCI(res2$prob, res2$ext.r2, t2, c(min(res2$r2), max(res1$r2)), redraw = T)
  plotMedCI(res1$prob, res1$r2, t1, c(0.2, 1))
  plotMedCI(res2$prob, res2$r2, t1, c(0.2, 1), redraw = T)
  plotMedCI(res1$prob, res1$ext.r2, t2, c(0.2, 1))
  plotMedCI(res2$prob, res2$ext.r2, t2, c(0.2, 1), redraw = T)
#  plotMedCI(res$prob, res$MPE)
#  plotMedCI(res$prob, res$ext.MPE)
}

plotMedCI <- function(prob, X, titl, ylime, redraw=F){
  meds <- tapply(X, prob, median)
  probs <- as.double(levels(as.factor(prob)))			  
  if (!redraw) {
	dashed <- 1
	plot(probs, meds, type="l", ylim=ylime, main=titl, lty=dashed)
	points(probs, meds, cex=0.6)
	print(meds)
	coleur <- "black" 		  
  }
  else { 	  
       	dashed <- 2
	lines(probs, meds, type="l", ylim=ylime, main=titl, col="black",
			lty=dashed)	
	points(probs, meds, cex=0.6)
	print(meds)
	coleur <- "black"
  }
  lineNinetyFivePercentIntervals(prob, X, coleur, dashed)
}	

lineNinetyFivePercentIntervals <- function(prob, X, coleur, dashed=1){
  probs <- as.double(levels(as.factor(prob)))	
  ms <- tapply(X, prob, mean)	
  sds <- tapply(X, prob, sd)	
  xm <- ms - (1.96 * sds)
  xp <- ms + (1.96 * sds)
  #plot
  lines(probs, xm, col=coleur, lty=dashed)
  lines(probs, xp, col=coleur, lty=dashed)
  print(xm)
  print(xp)
}	

plotAccuracyPrecision <- function(data.crossval, redraw, prefix, ps, segs = F){
  
  data.mpeSqr <- tapply(data.crossval$ext.MPE^2, data.crossval$prob, median)
  data.sdpeSqr <- tapply(data.crossval$ext.SDPE^2, data.crossval$prob, median)
  data.sdx <- tapply(data.crossval$ext.MPE^2, data.crossval$prob, mad)
  data.sdy <- tapply(data.crossval$ext.SDPE^2, data.crossval$prob, mad)
  
#  data.mpeSqr <- tapply(data.crossval[,7]^2, data.crossval$prob, mean)
#  data.sdpeSqr <- tapply(data.crossval[,8]^2, data.crossval$prob, mean)
# could have used mad but not used to it..
  
  if (redraw) plot(0,0, xlim=c(min(data.mpeSqr), max(data.mpeSqr)), 
	    ylim=c(min(data.sdpeSqr), max(data.sdpeSqr)),
	    xlab=expression(paste(MPE^2, " , ", Mg^2*m^-6)),
            ylab=expression(paste(SPDE^2, " , ", Mg^2*m^-6)))
  
  if (segs)
    for (i in ps*10){
#browser()    
	x <- data.mpeSqr[i]
	y <- data.sdpeSqr[i]
	print(c(x,y))
	sdx <- data.sdx[i]
	sdy <- data.sdy[i]
	segments(x-sdx, y, x+sdx, y, col="#525252", lty=4)
	segments(x, y-sdy, x, y+sdy, col="#525252", lty=4)
	print(paste("95%x", x-sdx, x+sdx, " 95%y", x-sdy, x+sdy))
    }
  
  for (i in ps*10){
	x <- data.mpeSqr[i]
	y <- data.sdpeSqr[i]
	sdx <- data.sdx[i]
	sdy <- data.sdy[i]
	text(data.mpeSqr[i], y,
	       label=paste(prefix, print(i), sep=""), cex=0.85)
  } 
}


