  # k=Ki[1]
  # cutoffValue = Ci[1]
  # num.thr = 15
  # Monfichier = paste0(elt.txt,'_BretMTOwMatPar_','C_',cutoffValue,'_S_',nsim,'_k_',k)
  
  # Fit the models TO -------------
  OptionVerbose  = TRUE
  OptionComputeSimul  = TRUE
  
  model = "total"
  source(paste0(path,'model/all/modelTOWeights.R') )
  
  summary(Myfit)
  
  save(mesh,mesh.st ,Myfit,mystack,form2,
       file=paste0(pathresultats,'/',"Fit",Monfichier,'.RData') )
  
  # Perform Simulations  -------------------
  samples=inla.posterior.sample(n = nsim,result = Myfit)
  
  names(samples[[1]])
  length(samples[[1]]$latent)
  head(samples[[1]]$latent)
  
  
  name.effects=c("intercept","matcl","FIRST_ENZ",
                 "space", "time", 
                 "spacetime","timeCovar")
  
  #dans cette liste, on mettra les simulations des différentes composantes du prédicteur
  sim.components=vector("list",length=length(name.effects))
  names(sim.components) <- name.effects
  
  for(i in 1:length(name.effects)){
    
    nch=nchar(name.effects[i])
    
    if (i !=1) {
      idx=which(substr(rownames(samples[[1]]$latent),1,nch+1)==paste0(name.effects[i],":"))
    } else {
      idx=which(substr(rownames(samples[[1]]$latent),1,nch)==name.effects[i])
    }
    length(idx)
    for(j in 1:nsim){
      sim.components[[i]]=cbind(sim.components[[i]],samples[[j]]$latent[idx,])
    }
  }
  
  dim(sim.components[[2]]) # les effets spatiaux simulés
  dim(sim.components[[3]]) # les effets spatiaux simulés
  dim(sim.components[[4]]) # les effets spatiaux simulés
  
  save(sim.components, 
       file=paste0(pathresultats,'/',"Simul",Monfichier,'.RData'))
  
  
  # Fit the models Degraded  -------------
  OptionVerbose  = FALSE
  OptionComputeSimul  = FALSE
  
  model = "degrad"
  source(paste0(path,'model/all/modelTOWeights.R') )
  # 
  tmp = summary(Myfit)
  save(tmp, 
       file=paste0(pathresultats,'/',"summaryModelTO_Degraded_",Monfichier,'.RData'))
  
  # 
  
  # Fit the models Degraded only ST -------------
  
  OptionVerbose  = FALSE
  OptionComputeSimul  = FALSE
  
  model = "degrade2"
  source(paste0(path,'model/all/modelTOWeights.R') )
  # 
  tmp = summary(Myfit)
  save(tmp, 
       file=paste0(pathresultats,'/',"summaryModelTO_Degraded2_",Monfichier,'.RData'))
  # 
  
