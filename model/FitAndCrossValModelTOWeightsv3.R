# 1 Packages-------------------------------------------------------
library(INLA)
library(sf)
library(dplyr)
library(sp)
library(foreach)
library(scoringRules)
library(gstat)
library(rgdal)
library(spacetime)

# inla.setOption("pardiso.license", "~/admin/pardiso.lic")
# inla.setOption(inla.mode="classic")
# inla.setOption(inla.mode="experimental")
inla.setOption(pardiso.license ="82F70E96BE7DA6A5956D4DF8F31E127ACCB33C981DE83F430BA469A2")

# inla.setOption(enable.inla.argument.weights=TRUE)


# Ces paramètres sont mis à jour dans la boucle du fichier maitre
# k <- 4 # 5+2 intervals
# cutoffValue = 0.35 # Cut off of the mesh
# elt.txt ='ph'
# 
# nsub=150000
# Data_zone <- Data_zone[sample(1:nrow(Data_zone) , nsub ),]

# Parameters ---------- 
source(paste0(path,'model/basicStats.R') )


# num.thr = 5
# nsim = 100 # For CRPS
# Define the mesh


Monfichier = paste0(elt.txt,'_',scale,'MTOwMatPar','_C_',cutoffValue,'_S_',nsim,'_k_',k)


load( paste0(pathdata,'data/Data',scale,'AcidMatPar',elt.txt,'.RData') )

table(Data_zone$annee)

set.seed(346)

print(Monfichier)


# Create the folding information for classical Xval  --------------

Data_zone <- Data_zone %>% 
mutate_at(vars(all_of(elt.txt) ), 
          .funs = funs(elt = MaTransform(.,elt.txt))) %>% # transformation
mutate(eltRaw = elt ) %>% # For the transformation, save the observed value
  mutate(val = elt ) %>% # For the validation, save the observed value
  mutate(pred = NA , # pour prediction ST Xval
         n2 = n /max(n))



#collect all data into a "stack":
##define intercept
Data_zone$intercept <- 1 # to avoid to use the INLA intercept 

if (elt %in% c("ph","ph2","ph3")) covarinla=Data_zone[,c("intercept","annee","mois","matcl","nlogCl","FIRST_ENZ")]
if (elt.txt=='PEquivOlsen') covarinla=Data_zone[,c("intercept","annee","matcl","nlogCl","FIRST_ENZ","PolsenFinalFlag")]
if (elt.txt=='carbone') covarinla=Data_zone[,c("intercept","annee","matcl","nlogCl","FIRST_ENZ")]
if (elt.txt=='mgo') covarinla=Data_zone[,c("intercept","annee","matcl","nlogCl","FIRST_ENZ")]
if (elt.txt=='k2o') covarinla=Data_zone[,c("intercept","annee","matcl","nlogCl","FIRST_ENZ")]

 # Convert to integer for INLA
covarinla$matcl = as.integer(covarinla$matcl)
covarinla$nlogCl = as.integer(covarinla$nlogCl)
covarinla$FIRST_ENZ = as.integer(covarinla$FIRST_ENZ)
if (elt.txt=='PEquivOlsen')covarinla$PolsenFinalFlag = as.integer(factor(covarinla$PolsenFinalFlag))


# transform the time into -.5:0.5
Data_zone$time01=(Data_zone$annee-min(Data_zone$annee))/(max(Data_zone$annee)-min(Data_zone$annee))
Data_zone$time01=Data_zone$time01-0.5
summary(Data_zone$time01)
dim(Data_zone)

# add a fixed effect for W1 with constraint = TRUE, mean = 0
covarinla$timeCovar = Data_zone$time01


# Cross ST  validation INLA -------------------
gc()

model = "total"

if( XvalOnly){
  
  
  resuXval <- 
    foreach(i = unique(Data_zone$fold),
            .errorhandling='pass' ) %do% {
              
              print(i)
              
              # Mettre en NA les individus pour la validation crois?e
              Data_zone$elt <- Data_zone$val
              Data_zone$elt[Data_zone$fold==i] <- NA
              
              
              OptionVerbose  = FALSE
              OptionComputeSimul  = TRUE
              
              source(paste0(path,'model/all/modelTOWeights.R') )
              
              fitted <- Myfit$summary.fitted.values$mean[1:length(mystack$data$data$y)]
              
              mask <- is.na(mystack$data$data$y)
              
              resu <- eval(MaBackTransform (fitted[mask],elt.txt),
                           MaBackTransform (Data_zone$val[Data_zone$fold==i],elt.txt )
              )
              
              
              Data_zone$pred[Data_zone$fold==i] <- fitted[mask]
              
              idx = which(Data_zone$fold==i) # mettre ici l'indice des observations utilisées avec inla qu'on cherche à prédire 
              # ATTENTION la ligne suivante peut prendre beaucoup de ressources : 
              sample = inla.posterior.sample(nsim, Myfit) 
              # length(sample)
              sims = list()
              for(s in 1:nsim){
                sims[[s]] = sample[[s]]$latent[idx]+
                  rnorm(length(idx), sd = 1/sqrt(Myfit$summary.hyperpar[1,1]))
              }
              
              
              predmat = matrix( MaBackTransform( unlist(sims ) , elt.txt), 
                                byrow = FALSE, ncol = nsim)
              
              crps_values = crps_sample(
                MaBackTransform(Data_zone$val[Data_zone$fold==i] , elt.txt) , 
                predmat) # put observed values "observed" here
              
              resu2 <- eval( rowMeans(predmat),
                             MaBackTransform (Data_zone$val[Data_zone$fold==i],elt.txt )
              )
              
              cbind(resu2,CRPS=mean(crps_values),resu )
            }
  
  resuXvalT <-  eval(Data_zone$pred,   Data_zone$val )
  
  save( resuXval , resuXvalT , Data_zone , 
        file=paste0(pathresultats,'/',"XvalST_",Monfichier,'.RData') )
  
  
  
  # Cross Classical  validation  -------------------
  gc()
  
  
  model = "total"
  
  
  resuXval <- 
    foreach(i = unique(Data_zone$fold2),
            .errorhandling='pass' ) %do% {
              
              print(i)
              
              Data_zone$elt <- Data_zone$val
              Data_zone$elt[Data_zone$fold2==i] <- NA
              
              
              OptionVerbose  = FALSE
              OptionComputeSimul  = TRUE
              source(paste0(path,'model/all/modelTOWeights.R') )
              
              gc()
              
              fitted <- Myfit$summary.fitted.values$mean[1:length(mystack$data$data$y)]
              
              mask <- is.na(mystack$data$data$y)
              
              
              resu <- eval(MaBackTransform (fitted[mask],elt.txt),
                           MaBackTransform (Data_zone$val[Data_zone$fold2==i],elt.txt )
              )
              
              Data_zone$pred[Data_zone$fold2==i] <- fitted[mask]
              
              idx = which(Data_zone$fold2==i) # mettre ici l'indice des observations utilisées avec inla qu'on cherche à prédire 
              
              # ATTENTION la ligne suivante peut prendre beaucoup de ressources : 
              sample = inla.posterior.sample(nsim, Myfit) 
              # length(sample)
              sims = list()
              for(s in 1:nsim){
                sims[[s]] = sample[[s]]$latent[idx]+rnorm(length(idx), sd = 1/sqrt(Myfit$summary.hyperpar[1,1]))
              }
              
              
              predmat = matrix(MaBackTransform( unlist(sims ) , elt.txt), 
                               byrow = FALSE, ncol = nsim)
              
              
              crps_values = crps_sample(
                MaBackTransform(Data_zone$val[Data_zone$fold2==i] , elt.txt) , 
                predmat) # put observed values "observed" here
              
              
              resu2 <- eval( rowMeans(predmat),
                             MaBackTransform (Data_zone$val[Data_zone$fold2==i],elt.txt )
              )
              
              cbind(resu2,CRPS=mean(crps_values),resu )
              
            }
  
  resuXvalT <-  eval(Data_zone$pred,   Data_zone$val )
  
  save( resuXval, resuXvalT , Data_zone , 
        file=paste0(pathresultats,'/',"XvalCla_",Monfichier,'.RData') )
  
  
  # Cross ST  validation  degrade -------------------
  gc()
  
  
  # resuXval <- 
  #   foreach(i = unique(Data_zone$fold),
  #           .errorhandling='pass' ) %do% {
  #             
  #             print(i)
  #             
  #             # Mettre en NA les individus pour la validation crois?e
  #             Data_zone$elt <- Data_zone$val
  #             Data_zone$elt[Data_zone$fold==i] <- NA
  #             
  #             
  #             OptionVerbose  = FALSE
  #             OptionComputeSimul  = TRUE
  #             
  #             source(paste0(path,'model/',elt.txt,'/modelTOdegrade',elt.txt,'.R') )
  #             
  #             fitted <- Myfit$summary.fitted.values$mean[1:length(mystack$data$data$y)]
  #             
  #             mask <- is.na(mystack$data$data$y)
  #             
  #             resu <- eval(MaBackTransform (fitted[mask],elt.txt),
  #                          MaBackTransform (Data_zone$val[Data_zone$fold==i],elt.txt )
  #             )
  #             
  #             
  #             Data_zone$pred[Data_zone$fold==i] <- fitted[mask]
  #             
  #             idx = which(Data_zone$fold==i) # mettre ici l'indice des observations utilisées avec inla qu'on cherche à prédire 
  #             # ATTENTION la ligne suivante peut prendre beaucoup de ressources : 
  #             sample = inla.posterior.sample(nsim, Myfit) 
  #             # length(sample)
  #             sims = list()
  #             for(s in 1:nsim){
  #               sims[[s]] = sample[[s]]$latent[idx]+rnorm(length(idx), sd = 1/sqrt(Myfit$summary.hyperpar[1,1]))
  #             }
  #             
  #             
  #             predmat = matrix(MaBackTransform( unlist(sims ) , elt.txt), 
  #                              byrow = FALSE, ncol = nsim)
  #             
  #             crps_values = crps_sample(
  #               MaBackTransform(Data_zone$val[Data_zone$fold==i] , elt.txt) , 
  #               predmat) # put observed values "observed" here
  #             
  #             
  #             resu2 <- eval( rowMeans(predmat),
  #                            MaBackTransform (Data_zone$val[Data_zone$fold==i],elt.txt )
  #             )
  #             
  #             cbind(resu2,CRPS=mean(crps_values),resu )
  #           }
  # 
  # resuXvalT <-  eval(Data_zone$pred,   Data_zone$val )
  # 
  # save( resuXval ,resuXvalT, Data_zone , file=paste0(path,'results/',"XvalST","Degrad",Monfichier,'.RData') )
  
  
  # Cross Classique validation degrade -------------------
  gc()
  
  model = "degrad"
  
  resuXval <- 
    foreach(i = unique(Data_zone$fold2),
            .errorhandling='pass' ) %do% {
              
              print(i)
              
              # Mettre en NA les individus pour la validation crois?e
              Data_zone$elt <- Data_zone$val
              Data_zone$elt[Data_zone$fold2==i] <- NA
              
              
              OptionVerbose  = FALSE
              OptionComputeSimul  = TRUE
              
              source(paste0(path,'model/all/modelTOWeights.R') )
              
              fitted <- Myfit$summary.fitted.values$mean[1:length(mystack$data$data$y)]
              
              mask <- is.na(mystack$data$data$y)
              
              resu <- eval(MaBackTransform (fitted[mask],elt.txt),
                           MaBackTransform (Data_zone$val[Data_zone$fold2==i],elt.txt )
              )
              
              
              Data_zone$pred[Data_zone$fold2==i] <- fitted[mask]
              
              idx = which(Data_zone$fold2==i) # mettre ici l'indice des observations utilisées avec inla qu'on cherche à prédire 
              # ATTENTION la ligne suivante peut prendre beaucoup de ressources : 
              sample = inla.posterior.sample(nsim, Myfit) 
              # length(sample)
              sims = list()
              for(s in 1:nsim){
                sims[[s]] = sample[[s]]$latent[idx]+rnorm(length(idx), sd = 1/sqrt(Myfit$summary.hyperpar[1,1]))
              }
              
              
              predmat = matrix(MaBackTransform( unlist(sims ) , elt.txt), 
                               byrow = FALSE, ncol = nsim)
              
              crps_values = crps_sample(
                MaBackTransform(Data_zone$val[Data_zone$fold2==i] , elt.txt) , 
                predmat) # put observed values "observed" here
              
              
              resu2 <- eval( rowMeans(predmat),
                             MaBackTransform (Data_zone$val[Data_zone$fold2==i],elt.txt )
              )
              
              cbind(resu2,CRPS=mean(crps_values),resu )
            }
  
  resuXvalT <-  eval(Data_zone$pred,   Data_zone$val )
  
  save( resuXval ,resuXvalT, Data_zone , 
        file=paste0(pathresultats,'/',"XvalCla_","Degrad_",Monfichier,'.RData') )
  
  
  # Cross ST  validation  degrade2 -------------------
  gc()
  
  
  # resuXval <- 
  #   foreach(i = unique(Data_zone$fold),
  #           .errorhandling='pass' ) %do% {
  #             
  #             print(i)
  #             
  #             # Mettre en NA les individus pour la validation crois?e
  #             Data_zone$elt <- Data_zone$val
  #             Data_zone$elt[Data_zone$fold==i] <- NA
  #             
  #             
  #             OptionVerbose  = FALSE
  #             OptionComputeSimul  = TRUE
  #             
  #             source(paste0(path,'model/',elt.txt,'/modelTOdegrade2',elt.txt,'.R') )
  #             
  #             fitted <- Myfit$summary.fitted.values$mean[1:length(mystack$data$data$y)]
  #             
  #             mask <- is.na(mystack$data$data$y)
  #             
  #             resu <- eval(MaBackTransform (fitted[mask],elt.txt),
  #                          MaBackTransform (Data_zone$val[Data_zone$fold==i],elt.txt )
  #             )
  #             
  #             
  #             Data_zone$pred[Data_zone$fold==i] <- fitted[mask]
  #             
  #             idx = which(Data_zone$fold==i) # mettre ici l'indice des observations utilisées avec inla qu'on cherche à prédire 
  #             # ATTENTION la ligne suivante peut prendre beaucoup de ressources : 
  #             sample = inla.posterior.sample(nsim, Myfit) 
  #             # length(sample)
  #             sims = list()
  #             for(s in 1:nsim){
  #               sims[[s]] = sample[[s]]$latent[idx]+rnorm(length(idx), sd = 1/sqrt(Myfit$summary.hyperpar[1,1]))
  #             }
  #             
  #             
  #             predmat = matrix(MaBackTransform( unlist(sims ) , elt.txt), 
  #                              byrow = FALSE, ncol = nsim)
  #             
  #             crps_values = crps_sample(
  #               MaBackTransform(Data_zone$val[Data_zone$fold==i] , elt.txt) , 
  #               predmat) # put observed values "observed" here
  #             
  #             
  #             resu2 <- eval( rowMeans(predmat),
  #                            MaBackTransform (Data_zone$val[Data_zone$fold==i],elt.txt )
  #             )
  #             
  #             cbind(resu2,CRPS=mean(crps_values),resu )
  #           }
  # 
  # resuXvalT <-  eval(Data_zone$pred,   Data_zone$val )
  # 
  # save( resuXval , resuXvalT , Data_zone , file=paste0(path,'results/',"XvalST","Degrad2",Monfichier,'.RData') )
  
  
  # Cross Classique  validation  degrade2 -------------------
  gc()
  
  model = "degrad2"
  
  resuXval <- 
    foreach(i = unique(Data_zone$fold2),
            .errorhandling='pass' ) %do% {
              
              print(i)
              
              # Mettre en NA les individus pour la validation crois?e
              Data_zone$elt <- Data_zone$val
              Data_zone$elt[Data_zone$fold2==i] <- NA
              
              
              OptionVerbose  = FALSE
              OptionComputeSimul  = TRUE
              
              source(paste0(path,'model/all/modelTOWeights.R') )
              
              fitted <- Myfit$summary.fitted.values$mean[1:length(mystack$data$data$y)]
              
              mask <- is.na(mystack$data$data$y)
              
              resu <- eval(MaBackTransform (fitted[mask],elt.txt),
                           MaBackTransform (Data_zone$val[Data_zone$fold2==i],elt.txt )
              )
              
              
              Data_zone$pred[Data_zone$fold2==i] <- fitted[mask]
              
              idx = which(Data_zone$fold2==i) # mettre ici l'indice des observations utilisées avec inla qu'on cherche à prédire 
              # ATTENTION la ligne suivante peut prendre beaucoup de ressources : 
              sample = inla.posterior.sample(nsim, Myfit) 
              # length(sample)
              sims = list()
              for(s in 1:nsim){
                sims[[s]] = sample[[s]]$latent[idx]+rnorm(length(idx), sd = 1/sqrt(Myfit$summary.hyperpar[1,1]))
              }
              
              
              predmat = matrix(MaBackTransform( unlist(sims ) , elt.txt), 
                               byrow = FALSE, ncol = nsim)
              
              crps_values = crps_sample(
                MaBackTransform(Data_zone$val[Data_zone$fold2==i] , elt.txt) , 
                predmat) # put observed values "observed" here
              
              
              resu2 <- eval( rowMeans(predmat),
                             MaBackTransform (Data_zone$val[Data_zone$fold2==i],elt.txt )
              )
              
              cbind(resu2,CRPS=mean(crps_values),resu )
            }
  
  resuXvalT <-  eval(Data_zone$pred,   Data_zone$val )
  
  save( resuXval , resuXvalT , Data_zone ,
        file=paste0(pathresultats,'/',"XvalCla_","Degrad2_",Monfichier,'.RData') )
  
  
}
  



# Fit the final models --------



if( !XvalOnly){
  
  k=Ki[1]
  cutoffValue = Ci[1]
  num.thr = 15
  Monfichier = paste0(elt.txt,'_',scale,'MTOwMatPar_','C_',cutoffValue,'_S_',nsim,'_k_',k)
  
  # Fit the models TO -------------
  OptionVerbose  = TRUE
  OptionComputeSimul  = TRUE
  
  model = "total"
  source(paste0(path,'model/all/modelTOWeights.R') )
  
  summary(Myfit)
  
  save(mesh,mesh.st ,Myfit,mystack,form2,
       file=paste0(pathresultats,'/',"Fit",Monfichier,'.RData') )
  
  # Perform Simulations  -------------------
  samples=inla.posterior.sample(n = nsim,result = Myfit)
  
  names(samples[[1]])
  length(samples[[1]]$latent)
  head(samples[[1]]$latent)
  
  
  name.effects=c("intercept","matcl","FIRST_ENZ",
                 "space", "time", 
                 "spacetime","timeCovar")

  #dans cette liste, on mettra les simulations des différentes composantes du prédicteur
  sim.components=vector("list",length=length(name.effects))
  names(sim.components) <- name.effects
  
  for(i in 1:length(name.effects)){
    
    nch=nchar(name.effects[i])
    
    if (i !=1) {
      idx=which(substr(rownames(samples[[1]]$latent),1,nch+1)==paste0(name.effects[i],":"))
    } else {
      idx=which(substr(rownames(samples[[1]]$latent),1,nch)==name.effects[i])
    }
    length(idx)
    for(j in 1:nsim){
      sim.components[[i]]=cbind(sim.components[[i]],samples[[j]]$latent[idx,])
    }
  }
  
  dim(sim.components[[2]]) # les effets spatiaux simulés
  dim(sim.components[[3]]) # les effets spatiaux simulés
  dim(sim.components[[4]]) # les effets spatiaux simulés
  
  save(sim.components, 
       file=paste0(pathresultats,'/',"Simul",Monfichier,'.RData'))
  
  
  # Fit the models Degraded  -------------
  OptionVerbose  = FALSE
  OptionComputeSimul  = FALSE
  
  source(paste0(path,'model/all','/modelTOdegrade2',elt.txt,'.R') )
  # 
  tmp = summary(Myfit)
  save(tmp, 
       file=paste0(path,'results/',"summaryModelTODegraded_",Monfichier,'.RData'))
  # 
  
  # Fit the models Degraded only ST -------------
  
  OptionVerbose  = FALSE
  OptionComputeSimul  = FALSE
  
  model = "total"
  source(paste0(path,'model/all/modelTOWeights.R') )
  # 
  tmp = summary(Myfit)
  save(tmp, 
       file=paste0(pathresultats,'/',"summaryModelTO_Degraded2_",Monfichier,'.RData'))
  # 
  
  
}

# Cross Classical  validation  -------------------
# gc()
# 
# 
# resuXvalClassique <- 
#   foreach(i = unique(Data_zone$fold2),.errorhandling='pass' ) %do% {
#     
#     print(i)
#     
#     Data_zone$elt <- Data_zone$val
#     Data_zone$elt[Data_zone$fold2==i] <- NA
#     
#     
#     OptionVerbose  = FALSE
#     OptionComputeSimul  = TRUE
#     
#     source(paste0(path,'model/modelTOdegrade',elt.txt,'.R') )
#     
#     gc()
#     
#     fitted <- Myfit$summary.fitted.values$mean[1:length(mystack$data$data$y)]
#     
#     mask <- is.na(mystack$data$data$y)
#     
#     resu <- summaryStats(fitted[mask],
#                          Data_zone$val[Data_zone$fold2==i] )
#     
#     
#     Data_zone$pred2[Data_zone$fold2==i] <- fitted[mask]
#     
#     idx = which(Data_zone$fold2==i) # mettre ici l'indice des observations utilisées avec inla qu'on cherche à prédire 
#     # ATTENTION la ligne suivante peut prendre beaucoup de ressources : 
#     
#     sample = inla.posterior.sample(nsim, Myfit) 
#     # length(sample)
#     sims = list()
#     for(s in 1:nsim){
#       sims[[s]] = sample[[s]]$latent[idx]+rnorm(length(idx), sd = 1/sqrt(Myfit$summary.hyperpar[1,1]))
#     }
#     
#     
#     predmat = matrix(unlist(sims), byrow = FALSE, ncol = nsim)
#     crps_values = crps_sample(Data_zone$val[Data_zone$fold2==i], predmat) # put observed values "observed" here
#     
#     
#     c(resu,CRPS=mean(crps_values) )
#     
#   }
# 
# save( resuXvalClassique , Data_zone , file=paste0(path,'results/',"XvalCla",'Degrad',Monfichier,'.RData') )
# 

