---
title: "Analyse multivariée"
author: "Nicolas Saby"
date: "`r Sys.Date()`"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(funHDDC)
library(sf)
library(dplyr)
library(tibble)
```

# Les données

On utilise les prédictions par pixels

```{r cars}
mgo.1 <- readRDS("results/mgo/Predictions_mgo_BretMTOwMatParXT_C_0.2_S_100_k_6.RData")
k2o.1 <- readRDS("results/k2o/Predictions_k2o_BretMTOwMatParXT_C_0.2_S_100_k_4.RData")
pequiv.1 <- readRDS("results/PEquivOlsen//Predictions_PEquivOlsen_BretMTOwMatParXT_C_0.2_S_100_k_4.RData")
carbone.1 <- readRDS("results/carbone/Predictions_carbone_BretMTOwMatParXT_C_0.2_S_100_k_8.RData")
ph.1 <- readRDS("results/ph2/Predictions_ph2_BretMTOwMatParXT_C_0.15_S_100_k_6.RData")

mgo <- na.omit(mgo.1)
k2o <- na.omit(k2o.1)
pequiv <- na.omit(pequiv.1)
carbone <- na.omit(carbone.1)
ph <- na.omit(ph.1)

# tirer au sort  n pixels
set.seed(32)
test <- sample(1:nrow(k2o),
               size =600 )

```

# le clustering de courbes

J'utilise le package library(funHDDC) qui s'appuie sur des courbes


## PKMg multivariate analysis

### Clustering


```{r pressure, echo=FALSE, fig.width = 17, fig.asp = .82}
basis <- create.bspline.basis(c(1991,2020),
                              nbasis=29)

var1 <- smooth.basis(argvals=seq(1991,2020,
                                 length.out = 30), 
                     y=t(mgo[test,2:31]),
                     fdParobj=basis)$fd

var2 <- smooth.basis(argvals=seq(1991,2020,
                                 length.out = 30), 
                     y=t(k2o[test,2:31]),
                     fdParobj=basis)$fd

var3 <- smooth.basis(argvals=seq(1991,2020,
                                 length.out = 30), 
                     y=t(carbone[test,2:31]),
                     fdParobj=basis)$fd

res.multipkmg <- funHDDC(list(var1,var2,var3),
                     K=4,
                     model=c("AkjBkQkDk","AkjBQkDk"),
                     init="kmeans",
                     threshold=0.2)

par(mfrow=c(3,1))
plot(var1, 
     col=res.multipkmg$class)
legend("topright",
       legend = paste("clust",1:4),
       col=1:4,
       lty = 1)
plot(var2, col=res.multipkmg$class)
legend("topright",
       legend = paste("clust",1:4),
       col=1:4,
       lty = 1)
plot(var3, col=res.multipkmg$class)
legend("topright",
       legend = paste("clust",1:4),
       col=1:4,
       lty = 1)

```
### Essais de cartographies

```{r}
xyPred <- readRDS("../data/grillePredWithFixedeffectBret.RDS")
predGrid <- xyPred %>% 
  filter(CODE_DEPT %in% c('22','29','35','56')) %>%
  st_as_sf(coords = c("x","y"))

predGidsel <- predGrid[test,]

predGidsel$pkmg <- factor(res.multipkmg$class)

library(tmap)
tm_shape(predGidsel) + 
  tm_dots(col = "pkmg",
          style = "fixed",
          size=1,
          palette=c("black","red","green3","blue")
          )



```

## pour le carbone et le ph

### Clustering

```{r, fig.width = 17, fig.asp = .82}
minA = 1996
maxA = 2019
n = 24
basis <- create.bspline.basis(c(minA,2019),
                              nbasis=n)


var3 <- smooth.basis(argvals=seq(minA,2019,
                                 length.out = n), 
                     y=t(carbone[test,7:30]),
                     fdParobj=basis)$fd

var4 <- smooth.basis(argvals=seq(minA,maxA,
                                 length.out = n), 
                     y=t(ph[test,2:25]),
                     fdParobj=basis)$fd

res.multicph <- funHDDC(list(var3 , var4),
                     K=4,
                     model=c("AkjBkQkDk","AkjBQkDk"),
                     init="kmeans",
                     threshold=0.02)

par(mfrow=c(2,1))
plot(var3, col=res.multicph$class)
legend("topright",
       legend = paste("clust",1:4),
       col=1:4,
       lty = 1)
plot(var4 ,col=res.multicph$class)
legend("topright",
       legend = paste("clust",1:4),
       col=1:4,
       lty = 1)

```

### Essais de cartographies

```{r}

predGidsel$cph <- factor(res.multicph$class)


tm_shape(predGidsel) + 
  tm_dots(col = "cph",
          style = "fixed",
          size=1,
          palette=c("black","red","green3","blue"))

```


# essai par gros pixels

```{r}
hex <- st_make_grid(predGrid,
                    cellsize=10000, 
                    square=FALSE) %>%
  st_sf() %>%
  rowid_to_column('hex_id')

GrilleHex <- st_join(hex,
                     predGrid,
                     largest=TRUE)

GrilleHex <- GrilleHex %>%
  filter(!is.na(FIRST_ENZ))
plot(GrilleHex[,"hex_id"])
```

```{r}
join <- st_join(predGrid,
                GrilleHex[,"hex_id"],
                largest=TRUE)
```


## Pour le Carbone, ph

```{r}

classes1<-aggregate(carbone.1[,-1],
                   by=list(join$hex_id),
                   median,
                   na.rm=TRUE)

classes2<-aggregate(ph.1[,-1],
                   by=list(join$hex_id),
                   median,
                   na.rm=TRUE)

classes1 %>%
  filter(!is.na(`Year.2014`)&!is.na(`Year.1996`)) %>%
  gather(annee, elt ,-Group.1)  %>%
  
  mutate(annee=as.numeric(str_sub(annee,6,10)) ) %>%
  ggplot() + 
  geom_line(aes(annee,elt,group=Group.1) , 
            col='grey',
            alpha=0.32, size=.2) +
  geom_smooth(aes(annee,elt ),method = lm, 
              formula = y ~ splines::bs(x, 3), 
              se = FALSE)+ 
  xlab("Year")+
  ylab("Carbone (g/kg)")+
  theme_bw()+  
  theme(axis.title.x = element_text(color="blue", 
                                    size=14, face="bold"),
                     axis.title.y = element_text(color="blue",
                                                 size=12, 
                                                 face="bold"),
                     axis.text = element_text(size=10))+
  scale_x_continuous(breaks = c(1991:2014),
                     limits=c(1991,2014))
  

```


```{r}

minA = 1996
maxA = 2019
n = 24
basis <- create.bspline.basis(c(minA,2019),
                              nbasis=n)


var3 <- smooth.basis(argvals=seq(minA,2019,
                                 length.out = n), 
                     y=t(classes1[,7:30]),
                     fdParobj=basis)$fd

var4 <- smooth.basis(argvals=seq(minA,maxA,
                                 length.out = n), 
                     y=t(classes2[,2:25]),
                     fdParobj=basis)$fd

res.multi <- funHDDC(list(var3 , var4),
                     K=3,
                     model=c('AkjBkQkDk',
                             'AkjBQkDk',
                             'AkBkQkDk',
                             'ABkQkDk', 
                             'AkBQkDk', 
                             'ABQkDk'),
                     init="kmeans",
                     threshold=0.001,
                     eps = 1e-9)

# par(mfrow=c(2,1))
# plot(var3, col=res.multi$class)
# plot(var4 ,col=res.multi$class)

res.multi

```

## Pour le pkmg



```{r}
classes1<-aggregate(mgo.1[,-1],
                   by=list(join$hex_id),
                   median,
                   na.rm=TRUE)

classes2<-aggregate(k2o.1[,-1],
                   by=list(join$hex_id),
                   median,
                   na.rm=TRUE)

classes3<-aggregate(pequiv.1[,-1],
                   by=list(join$hex_id),
                   median,
                   na.rm=TRUE)

classes3 %>%
  filter(!is.na(`Year.2014`)&!is.na(`Year.1996`)) %>%
  gather(annee, elt ,-Group.1)  %>%
  
  mutate(annee=as.numeric(str_sub(annee,6,10)) ) %>%
  ggplot() + 
  geom_line(aes(annee,elt,group=Group.1) , 
            col='grey',alpha=0.32, size=.2) +
  geom_smooth(aes(annee,elt ),method = lm, 
              formula = y ~ splines::bs(x, 3), 
              se = FALSE)+ 
  xlab("Year")+
  ylab("Phosphore (mg/kg)")+
  theme_bw()+  
  theme(axis.title.x = element_text(color="blue", 
                                    size=14, face="bold"),
                     axis.title.y = element_text(color="blue",
                                                 size=12, 
                                                 face="bold"),
                     axis.text = element_text(size=10))+
  scale_x_continuous(breaks = c(1991:2014),
                     limits=c(1991,2014))
  

```

Clustering

```{r}

basis <- create.bspline.basis(c(1991,2020),
                              nbasis=30)

var1 <- smooth.basis(argvals=seq(1991,2020,
                                 length.out = 30), 
                     y=t(classes1[,2:31]),
                     fdParobj=basis)$fd

var2 <- smooth.basis(argvals=seq(1991,2020,
                                 length.out = 30), 
                     y=t(classes2[,2:31]),
                     fdParobj=basis)$fd

var3 <- smooth.basis(argvals=seq(1991,2020,
                                 length.out = 30), 
                     y=t(classes3[,2:31]),
                     fdParobj=basis)$fd

res.multi <- funHDDC(list(var1,var2,var3),
                     K=4,
                     model=c("AkjBkQkDk","AkjBQkDk"),
                     init="kmeans",
                     threshold=0.2)

# par(mfrow=c(3,1))
# plot(var1, col=res.multi$class)
# plot(var2, col=res.multi$class)
# plot(var3, col=res.multi$class)

res.multi

```
