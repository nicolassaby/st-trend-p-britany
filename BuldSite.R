library(rmarkdown)
library(foreach)
library(stringr)
library(prettydoc)
library(rmdformats)



rmarkdown::render("index.Rmd", 
                  html_pretty(theme= 'leonids',
                              toc=TRUE,
                              number_sections = TRUE
                  ),
                  output_dir = 'public/',
                  output_file = 'index.html',
                  # output_format = c("html_document"),
                  encoding="UTF-8"
)




rmarkdown::render("CrossValidv2.Rmd", 
                  output_dir = 'public/',
                  output_file = paste0('ph','OuputMapsanrResults.html'
                  ),
                  params = list( elt.txt = "ph" ) ,
                  
                  readthedown(highlight = "kate", 
                              gallery = TRUE,
                              fig_width = 12,
                              fig_height = 12,
                              # toc = TRUE,
                              # toc_float = TRUE,
                              code_folding = c("hide")
                  ),
                  # output_format = c("html_document"),
                  encoding="UTF-8"
)


# pH sans mois

elt = "ph2"
rmarkdown::render("CrossValidv2.Rmd", 
                  output_dir = 'public/',
                  output_file = paste0(elt,'OuputMapsanrResults.html'
                  ),
                  params = list( elt.txt = elt ) ,
                  
                  readthedown(highlight = "kate", 
                              gallery = TRUE,
                              fig_width = 12,
                              fig_height = 12,
                              # toc = TRUE,
                              # toc_float = TRUE,
                              code_folding = c("hide")
                  ),
                  # output_format = c("html_document"),
                  encoding="UTF-8" 
)


elt = "ph3"
rmarkdown::render("CrossValidv2.Rmd", 
                  output_dir = 'public/',
                  output_file = paste0(elt,'OuputMapsanrResults.html'
                  ),
                  params = list( elt.txt = elt ) ,
                  
                  readthedown(highlight = "kate", 
                              gallery = TRUE,
                              fig_width = 12,
                              fig_height = 12,
                              # toc = TRUE,
                              # toc_float = TRUE,
                              code_folding = c("hide")
                  ),
                  # output_format = c("html_document"),
                  encoding="UTF-8" 
)

rmarkdown::render("CrossValidv2.Rmd", 
                  output_dir = 'public/',
                  output_file = paste0('mgo','OuputMapsanrResults.html'
                  ),
                  params = list( elt.txt = "mgo" ) ,
                  
                  readthedown(highlight = "kate", 
                              gallery = TRUE,
                              fig_width = 12,
                              fig_height = 12,
                              # toc = TRUE,
                              # toc_float = TRUE,
                              code_folding = c("hide")
                  ),
                  # output_format = c("html_document"),
                  encoding="UTF-8"
)



# Carbone
elt = "carbone"
rmarkdown::render("CrossValidv2.Rmd", 
                  output_dir = 'public/',
                  output_file = paste0(elt,'OuputMapsanrResults.html'
                  ),
                  params = list( elt.txt = elt ) ,
                  
                  readthedown(highlight = "kate", 
                              gallery = TRUE,
                              fig_width = 12,
                              fig_height = 12,
                              # toc = TRUE,
                              # toc_float = TRUE,
                              code_folding = c("hide")
                  ),
                  # output_format = c("html_document"),
                  encoding="UTF-8" 
                  )



elt = "k2o"
rmarkdown::render("CrossValidv2.Rmd", 
                  output_dir = 'public/',
                  output_file = paste0(elt,'OuputMapsanrResults.html'
                  ),
                  params = list( elt.txt = elt ) ,
                  
                  readthedown(highlight = "kate", 
                              gallery = TRUE,
                              fig_width = 12,
                              fig_height = 12,
                              # toc = TRUE,
                              # toc_float = TRUE,
                              code_folding = c("hide")
                  ),
                  # output_format = c("html_document"),
                  encoding="UTF-8" 
)


# Pequivolsen

elt = "PEquivOlsen"
rmarkdown::render("CrossValidv2.Rmd", 
                  output_dir = 'public/',
                  output_file = paste0(elt,'OuputMapsanrResults.html'
                  ),
                  params = list( elt.txt = elt ) ,
                  
                  readthedown(highlight = "kate", 
                              gallery = TRUE,
                              fig_width = 12,
                              fig_height = 12,
                              # toc = TRUE,
                              # toc_float = TRUE,
                              code_folding = c("hide")
                  ),
                  # output_format = c("html_document"),
                  encoding="UTF-8" 
)


elt = "PEquivOlsen"
rmarkdown::render("CrossValidv2.Rmd", 
                  output_dir = 'public/',
                  output_file = paste0(elt,'OuputMapsanrResultsFRA.html'
                  ),
                  params = list( elt.txt = elt , scale = "FRA") ,
                  
                  readthedown(highlight = "kate", 
                              gallery = TRUE,
                              fig_width = 12,
                              fig_height = 12,
                              # toc = TRUE,
                              # toc_float = TRUE,
                              code_folding = c("hide")
                  ),
                  # output_format = c("html_document"),
                  encoding="UTF-8" 
)

rmarkdown::render("MultivariateAbna.Rmd", 
                  html_pretty(theme= 'leonids',
                              toc=TRUE,
                              number_sections = TRUE
                  ),
                  output_dir = 'public/',
                  output_file = 'MV.html',
                  readthedown(highlight = "kate", 
                              gallery = TRUE,
                              fig_width = 12,
                              fig_height = 12,
                              # toc = TRUE,
                              # toc_float = TRUE,
                              code_folding = c("hide")
                  ),
                  encoding="UTF-8"
)


