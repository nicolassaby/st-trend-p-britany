---
title: "Pari Scientifique ST BDAT"
author:
  - Nicolas Saby^[INRAe Infosol, nicolas.saby[at]inrae.fr]
  - Thomas Opitz^[INRAe BioSp]
  - Hocine Bourennane^[INRAE UR Sol]
date: "`r Sys.Date()`"
output: 
  prettydoc::html_pretty:
    theme: cayman
    highlight: github
    math: katex
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
![ST predictions](images/predP.gif)

This website presents the results of the INLA project Pari scientifique.

Chaque élement dans la liste suivante



 * <a href="carboneOuputMapsanrResults.html"> Le carbone  </a> (30 ans) ,
 
<BR>

 * <a href="phOuputMapsanrResults.html"> Le pH , </a> (25 ans), avec l'effet du mois de prélevement,
 
 <BR>

 * <a href="ph2OuputMapsanrResults.html"> Le pH , </a> (25 ans), SANS l'effet du mois de prélevement,
 
 <BR>

 * <a href="k2oOuputMapsanrResults.html"> Le K2O , </a> (30 ans)

<BR>

 * <a href="mgoOuputMapsanrResults.html"> Le MgO , </a> (30 ans)

 
<BR>

 * <a href="PEquivOlsenOuputMapsanrResults.html"> Le P Equiv Olsen </a>  (30 ans) 

<BR>

Une analyse multivariée <a href="MV.html"> ici </a> 

