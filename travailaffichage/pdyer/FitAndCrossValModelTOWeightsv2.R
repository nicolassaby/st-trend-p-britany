# 1 Packages-------------------------------------------------------
library(INLA)
library(sf)
library(dplyr)
library(sp)
library(foreach)
library(scoringRules)

inla.setOption("pardiso.license", "~/admin/pardiso.lic")

inla.setOption(enable.inla.argument.weights=TRUE)

load('~/pari/data/DataBretagnePdyer.RData')

source('basicStats.R')



## Parameters ---------- 
num.thr = 14

k <- 8 # 5+2 intervals

nsim = 200 # For CRPS

# Define the mesh
cutoffValue = 0.15 # Cut off of the mesh
bound.outer = 1.5

elt ='PDyer'

Monfichier = paste0(elt,'BretMTOw','C',cutoffValue,'S',nsim,'k',k)

# nsub=150000
# Data_zone <- Data_zone[sample(1:nrow(Data_zone) , nsub ),]


table(Data_zone$annee)

set.seed(345)

print(Monfichier)

# Create the folding information for classical Xval  --------------

Data_zone <- Data_zone %>% 
  mutate(p = elt  ) %>% # For the validation, elt is the column used in the code for inla 
  mutate(elt = sqrt(p)  ) %>% # For the validation, elt is the column used in the code for inla 
  mutate(val = elt ) %>% # For the validation, save the observed value
  mutate(pred = NA , # pour prediction ST Xval
         pred2 = NA, # pour prediction Classical Xval
         n2 = n /max(n))


# Fit the models TO -------------
OptionVerbose  = TRUE
OptionComputeSimul  = TRUE

source('modelTOWeights.R')

summary(Myfit)

save(mesh,mesh.st ,Myfit,mystack,
     file=paste0("Fit",Monfichier,'.RData') )

# Perform Simulations  -------------------
samples=inla.posterior.sample(nsim,Myfit)

names(samples[[1]])
length(samples[[1]]$latent)
head(samples[[1]]$latent)


name.effects=c("intercept","matcl","FIRST_ENZ", "space", "time", "spacetime")

#dans cette liste, on mettra les simulations des différentes composantes du prédicteur
sim.components=vector("list",length=length(name.effects))
names(sim.components) <- name.effects

for(i in 1:length(name.effects)){
  
  nch=nchar(name.effects[i])
  
  if (i !=1) {
    idx=which(substr(rownames(samples[[1]]$latent),1,nch+1)==paste0(name.effects[i],":"))
  } else {
    idx=which(substr(rownames(samples[[1]]$latent),1,nch)==name.effects[i])
  }
  length(idx)
  for(j in 1:nsim){
    sim.components[[i]]=cbind(sim.components[[i]],samples[[j]]$latent[idx,])
  }
}

dim(sim.components[[2]]) # les effets spatiaux simulés
dim(sim.components[[3]]) # les effets spatiaux simulés
dim(sim.components[[4]]) # les effets spatiaux simulés

save(sim.components, file=paste0("Simul",Monfichier,'.RData'))


# Fit the models Degraded  -------------
OptionVerbose  = FALSE
OptionComputeSimul  = FALSE

source('modelTOdegrade.R')
# 
summary(Myfit)
# 



# Cross ST  validation  -------------------
gc()

#setwd("C:/Users/nsaby/SharePoint/Pari Scientifique - Documents 1/scripts/crossvalidation/")

resuXval2 <- 
  foreach(i = unique(Data_zone$fold),
          .errorhandling='pass' ) %do% {
            
            print(i)
            
            # Mettre en NA les individus pour la validation crois?e
            Data_zone$elt <- Data_zone$val
            Data_zone$elt[Data_zone$fold==i] <- NA
            
            
            OptionVerbose  = FALSE
            OptionComputeSimul  = TRUE
            source('modelTOWeights.R')
            
            fitted <- Myfit$summary.fitted.values$mean[1:length(mystack$data$data$y)]
            
            mask <- is.na(mystack$data$data$y)
            
            resu <- summaryStats(fitted[mask],
                                 Data_zone$val[Data_zone$fold==i] )
            
            
            Data_zone$pred[Data_zone$fold==i] <- fitted[mask]
            
            idx = which(Data_zone$fold==i) # mettre ici l'indice des observations utilisées avec inla qu'on cherche à prédire 
            # ATTENTION la ligne suivante peut prendre beaucoup de ressources : 
            sample = inla.posterior.sample(nsim, Myfit) 
            # length(sample)
            sims = list()
            for(s in 1:nsim){
              sims[[s]] = sample[[s]]$latent[idx]+rnorm(length(idx), sd = 1/sqrt(Myfit$summary.hyperpar[1,1]))
            }
            
            
            predmat = matrix(unlist(sims), byrow = FALSE, ncol = nsim)
            crps_values = crps_sample(Data_zone$val[Data_zone$fold==i], predmat) # put observed values "observed" here
            
            
            c(resu,CRPS=mean(crps_values) )
          }

save( resuXval2 , Data_zone , file=paste0("XvalST",Monfichier,'.RData') )


# Cross Classical  validation  -------------------
gc()




resuXvalClassique <- 
  foreach(i = unique(Data_zone$fold2),.errorhandling='pass' ) %do% {
    
    print(i)
    
    Data_zone$elt <- Data_zone$val
    Data_zone$elt[Data_zone$fold2==i] <- NA
    
    
    OptionVerbose  = FALSE
    OptionComputeSimul  = TRUE
    source('modelTOWeights.R')
    
    gc()
    
    fitted <- Myfit$summary.fitted.values$mean[1:length(mystack$data$data$y)]
    
    mask <- is.na(mystack$data$data$y)
    
    resu <- summaryStats(fitted[mask],
                         Data_zone$val[Data_zone$fold2==i] )
    
    
    Data_zone$pred2[Data_zone$fold2==i] <- fitted[mask]
    
    idx = which(Data_zone$fold2==i) # mettre ici l'indice des observations utilisées avec inla qu'on cherche à prédire 
    
    # ATTENTION la ligne suivante peut prendre beaucoup de ressources : 
    sample = inla.posterior.sample(nsim, Myfit) 
    # length(sample)
    sims = list()
    for(s in 1:nsim){
      sims[[s]] = sample[[s]]$latent[idx]+rnorm(length(idx), sd = 1/sqrt(Myfit$summary.hyperpar[1,1]))
    }
    
    
    predmat = matrix(unlist(sims), byrow = FALSE, ncol = nsim)
    crps_values = crps_sample(Data_zone$val[Data_zone$fold2==i], predmat) # put observed values "observed" here
    
    
    c(resu,CRPS=mean(crps_values) )
    
  }

save( resuXvalClassique , Data_zone , file=paste0("XvalCla",Monfichier,'.RData') )


# Cross ST  validation  degrade -------------------
gc()


resuXval2 <- 
  foreach(i = unique(Data_zone$fold),
          .errorhandling='pass' ) %do% {
            
            print(i)
            
            # Mettre en NA les individus pour la validation crois?e
            Data_zone$elt <- Data_zone$val
            Data_zone$elt[Data_zone$fold==i] <- NA
            
            
            OptionVerbose  = FALSE
            OptionComputeSimul  = TRUE
            source('modelTOdegrade.R')
            
            fitted <- Myfit$summary.fitted.values$mean[1:length(mystack$data$data$y)]
            
            mask <- is.na(mystack$data$data$y)
            
            resu <- summaryStats(fitted[mask],
                                 Data_zone$val[Data_zone$fold==i] )
            
            
            Data_zone$pred[Data_zone$fold==i] <- fitted[mask]
            
            idx = which(Data_zone$fold==i) # mettre ici l'indice des observations utilisées avec inla qu'on cherche à prédire 
            # ATTENTION la ligne suivante peut prendre beaucoup de ressources : 
            sample = inla.posterior.sample(nsim, Myfit) 
            # length(sample)
            sims = list()
            for(s in 1:nsim){
              sims[[s]] = sample[[s]]$latent[idx]+rnorm(length(idx), sd = 1/sqrt(Myfit$summary.hyperpar[1,1]))
            }
            
            
            predmat = matrix(unlist(sims), byrow = FALSE, ncol = nsim)
            crps_values = crps_sample(Data_zone$val[Data_zone$fold==i], predmat) # put observed values "observed" here
            
            
            c(resu,CRPS=mean(crps_values) )
          }

save( resuXval2 , Data_zone , file=paste0("XvalST","Degrad",Monfichier,'.RData') )


# Cross Classical  validation  -------------------
gc()


resuXvalClassique <- 
  foreach(i = unique(Data_zone$fold2),.errorhandling='pass' ) %do% {
    
    print(i)
    
    Data_zone$elt <- Data_zone$val
    Data_zone$elt[Data_zone$fold2==i] <- NA
    
    
    OptionVerbose  = FALSE
    OptionComputeSimul  = TRUE
    source('modelTOdegrade.R')
    
    gc()
    
    fitted <- Myfit$summary.fitted.values$mean[1:length(mystack$data$data$y)]
    
    mask <- is.na(mystack$data$data$y)
    
    resu <- summaryStats(fitted[mask],
                         Data_zone$val[Data_zone$fold2==i] )
    
    
    Data_zone$pred2[Data_zone$fold2==i] <- fitted[mask]
    
    idx = which(Data_zone$fold2==i) # mettre ici l'indice des observations utilisées avec inla qu'on cherche à prédire 
    # ATTENTION la ligne suivante peut prendre beaucoup de ressources : 
    
    sample = inla.posterior.sample(nsim, Myfit) 
    # length(sample)
    sims = list()
    for(s in 1:nsim){
      sims[[s]] = sample[[s]]$latent[idx]+rnorm(length(idx), sd = 1/sqrt(Myfit$summary.hyperpar[1,1]))
    }
    
    
    predmat = matrix(unlist(sims), byrow = FALSE, ncol = nsim)
    crps_values = crps_sample(Data_zone$val[Data_zone$fold2==i], predmat) # put observed values "observed" here
    
    
    c(resu,CRPS=mean(crps_values) )
    
  }

save( resuXvalClassique , Data_zone , file=paste0("XvalCla",'Degrad',Monfichier,'.RData') )


