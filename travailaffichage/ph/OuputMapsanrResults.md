---
title: "INLA results"
author: "Nicolas Saby"
date: "19/01/2022"
output:
  html_document: 
    keep_md: yes
    toc: yes
    toc_float: yes
    number_sections: yes
    fig_width: 11
    fig_height: 7
    fig_caption: yes
---

<style type="text/css">
h1, #TOC>ul>li {
  color: #ff0000;
}

h2, #TOC>ul>ul>li {
  color: #F4B5BD;
  font-family:  "Times";
  font-weight: bold;
}
</style>




# Les données brutes




Ce document concerne les données suivante phBretMTOwC0.05S100k6

## dans l'espace et le temps

la répartition des observations

![](OuputMapsanrResults_files/figure-html/carte-1.png)<!-- -->

## dans le temps uniquement

Et enfin une reprsentation dans le temps de la variabilité des données

![](OuputMapsanrResults_files/figure-html/unnamed-chunk-2-1.png)<!-- -->![](OuputMapsanrResults_files/figure-html/unnamed-chunk-2-2.png)<!-- -->

## la triangulation

Voici le mesh choisi

![](OuputMapsanrResults_files/figure-html/unnamed-chunk-3-1.png)<!-- -->

## le modèle

Finalement le modèle utilisé

\begin{equation}\label{eq:mod-notrend}
    Y(s,t)=\beta_0+\sum_{i\in \text{scorpan}}\beta^{\text{sc}}_i z^{\text{sc}}_i(s)+f(\text{month}(t))+W_0(s)+(\tilde{t}-0.5)W_1(s)+\varepsilon(s,t),
    \end{equation}

ici pas de $f(\text{month}(t))$

# La validation croisée 


```{=html}
<div id="htmlwidget-c0f71d85cc119dc8e7e0" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-c0f71d85cc119dc8e7e0">{"x":{"filter":"none","vertical":false,"data":[["1","2","3","4"],["XvalST","XvalCla","XvalSTDegrad","XvalSTDegrad2"],[0,0,0,-0.01],[0.34,0.34,0.33,0.32],[0.45,0.45,0.44,0.42],[0.4,0.4,0.42,0.46],[0.16,0.16,0.18,0.22],[-0.88,-0.88,-1.05,-1.04],[0.37,0.37,0.39,0.42],[0.94,0.94,0.92,0.9]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>meth<\/th>\n      <th>ME<\/th>\n      <th>MAE<\/th>\n      <th>RMSE<\/th>\n      <th>r<\/th>\n      <th>r2<\/th>\n      <th>NSE<\/th>\n      <th>rhoC<\/th>\n      <th>Cb<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"columnDefs":[{"className":"dt-right","targets":[2,3,4,5,6,7,8,9]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script>
```
# Les sorties brutes

## Les hyper paramètres

### les effets spatio temporels

$W_0(s)$ et $W_1(s)$

![](OuputMapsanrResults_files/figure-html/unnamed-chunk-5-1.png)<!-- -->![](OuputMapsanrResults_files/figure-html/unnamed-chunk-5-2.png)<!-- -->

### Les effet "fixes"

$$\sum_{i\in \text{scorpan}}\beta^{\text{sc}}_i$$


![](OuputMapsanrResults_files/figure-html/unnamed-chunk-6-1.png)<!-- -->![](OuputMapsanrResults_files/figure-html/unnamed-chunk-6-2.png)<!-- -->

L'effet mois

![](OuputMapsanrResults_files/figure-html/unnamed-chunk-7-1.png)<!-- -->


## les cartographies
### l'évolution $W_1(s)$

La tendance temporelle $W_1(s)$, ici on cartographie la médiane de la posterieur.

![](OuputMapsanrResults_files/figure-html/unnamed-chunk-8-1.png)<!-- -->

on peut cartographier l'incertitude associée en utilisant les simulations

![](OuputMapsanrResults_files/figure-html/unnamed-chunk-9-1.png)<!-- -->


Il est donc possible d'inférer si la valeur 0 est incluse dans l'intervalle de crédibilité ou dans l'intervalle calculé par les simulations

![](OuputMapsanrResults_files/figure-html/unnamed-chunk-10-1.png)<!-- -->

### La tendance globale $W_0(s)$

on ajoute le $W_0(s)$ et l'intercept $\beta_0$
De la même manière, on peut carctériser l'incertitude

![](OuputMapsanrResults_files/figure-html/unnamed-chunk-11-1.png)<!-- -->


# les prédictions annuelles

Il faut préparer la grille pour la rendre compatible avec inla qui ne gère pas bien les facteurs R


```
## 
##    3    5 
## 3131 1195
```

```
## 
##    1    2 
## 3131 1195
```

```
## 
##    1    2    4    5    7    9 
##  126    5    8  347 3847    1
```

Il faut ensuite produire les prédictions pour chaque $k$ en chaque pixels $s$

Pour cela, il faut également résumé les simulations de chaque effet



On peut ensuite produire une carte pour un $t$ choisi, ici $t =  2014$

![](OuputMapsanrResults_files/figure-html/unnamed-chunk-14-1.png)<!-- -->

Avec le package tmap

![](OuputMapsanrResults_files/figure-html/unnamed-chunk-15-1.png)<!-- -->

construction du raster pour l'animation



```
## Creating frames
```

```
## =======
```

```
## ===
```

```
## ===
```

```
## ====
```

```
## ===
```

```
## ===
```

```
## ====
```

```
## ===
```

```
## ===
```

```
## ====
```

```
## ===
```

```
## ===
```

```
## ====
```

```
## ===
```

```
## ===
```

```
## ====
```

```
## ===
```

```
## ===
```

```
## ====
```

```
## ===
```

```
## ===
```

```
## ====
```

```
## ===
```

```
## 
## Creating animation
## Animation saved to D:\projets\bdat\analyses\evolution\st-trend-p-britany\ph\predP.gif
```

Une petite animation

![](OuputMapsanrResults_files/figure-html/unnamed-chunk-17-1.gif)<!-- -->
