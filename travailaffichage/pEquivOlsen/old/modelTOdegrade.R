# 1 Prepare the data and the meshes ----------------

source('DefineMesh.R')

# 2 Define the effects -------------------------------------------------

# transform the time into -.5:0.5
Data_zone$time01=(Data_zone$annee-min(Data_zone$annee))/(max(Data_zone$annee)-min(Data_zone$annee))
Data_zone$time01=Data_zone$time01-0.5
summary(Data_zone$time01)
dim(Data_zone)


## Define the matern covariance
nu=.5 #regularity parameter of Mat�rn
D=2 # dimension
alpha = nu + D/2


# define spatial Mat�rn fields
spde.space.Ws = inla.spde2.pcmatern(mesh = mesh,
                                    alpha = alpha,
                                    prior.range=c(.25,.5),# set second number to NA to not fit the hyperparameter
                                    prior.sigma=c(1,.5), # set second number to NA to not fit the hyperparameter
                                    constr=FALSE)

spde.time.Wt = inla.spde2.pcmatern(mesh = mesh,
                                   alpha = alpha,
                                   prior.range=c(.25,.5),# set second number to NA to not fit the hyperparameter
                                   prior.sigma=c(1,.5), # set second number to NA to not fit the hyperparameter
                                   constr=FALSE)

# spde.residuals.Wst =inla.spde2.pcmatern(mesh=mesh.st,
#                                         alpha=alpha,
#                                         prior.range=c(.25,.5),# set second number to NA to not fit the hyperparameter
#                                         prior.sigma=c(1,.5),# set second number to NA to not fit the hyperparameter
#                                         constr=FALSE)

#make spatial index that runs through the spatial discretization points (=triangulation nodes):
# for the spatially varying mean
idx.space=inla.spde.make.index("space",
                               n.spde=spde.space.Ws$n.spde,
                               n.group=1)
# for the spatially variying regression
idx.time=inla.spde.make.index("time",
                              n.spde=spde.time.Wt$n.spde,
                              n.group=1)
# # for the spatio temporal residuals
# idx.ST=inla.spde.make.index("spacetime", 
#                             spde.residuals.Wst$n.spde,
#                             n.group=k+2
#) # k interval + 2 extrems

# Define the A matrices
# to link coordinates of observed data to the spatial discretization points:

A.space=inla.spde.make.A(mesh,
                         loc=xyMesh,
                         index=1:nrow(xyMesh),
                         n.group=1)

A.time=inla.spde.make.A(mesh,
                        loc=xyMesh,
                        index=1:nrow(xyMesh),
                        n.group=1)

A.time = A.time * Data_zone$time01


# A.st=inla.spde.make.A(mesh.st,
#                       loc=xyMesh,
#                       index=1:nrow(xyMesh),
#                       group.mesh=mesh.time,
#                       group=Data_zone$time01)

#sum(A2 != 0)
#sum(is.na(A2))

#collect all data into a "stack":

##define intercept
Data_zone$intercept <- 1 # to avoid to use the INLA intercept 

#covarinla=Data_zone[,c("intercept","annee","mois","matcl","nlogCl","FIRST_ENZ")]
covarinla=Data_zone[,c("intercept","annee","matcl","nlogCl","FIRST_ENZ","PolsenFinalFlag")]

# Convert to integer for INLA
covarinla$matcl = as.integer(covarinla$matcl)
covarinla$nlogCl = as.integer(covarinla$nlogCl)
covarinla$FIRST_ENZ = as.integer(covarinla$FIRST_ENZ)
covarinla$PolsenFinalFlag = as.integer(covarinla$PolsenFinalFlag)

str(covarinla)
# scale if necessary
#for(i in 1:ncol(covarinla)){
#  covarinla[,i]=scale(covarinla[,i])
#}


mystack=inla.stack(data=list(y=Data_zone$elt),
                   A=list(A.space,
                          A.time,
                          # A.st,
                          1),
                   effects=list(idx.space,
                                idx.time,
                                # idx.ST,
                                covarinla),
                   tag="obs")## compiled data and model parameters

#rm(Data_zone,covarinla,A1,A2,idx.spatial1,idx.spatial2)
rm(covarinla,A.space,A.st,A.time,idx.space,idx.time,idx.ST)##deleted redundancy objects
gc()

## 3 define model parameter ---------------



form2 = y ~
  -1 + intercept +
  f(nlogCl,model="iid",
    constr=TRUE , 
    hyper = list(theta = list(prior = "pc.prec",
                              param = c( .5 , NA ) ) # on utilise la pc.prec mais avec NA , on fixe la valeur de ec de .2
    )) +
  f(matcl,model="iid",
    constr=TRUE , 
    hyper = list(theta = list(prior = "pc.prec",
                              param = c( .05 , NA ) ) # on utilise la pc.prec mais avec NA , on fixe la valeur de ec de .2
    )) +
  f(FIRST_ENZ,model="iid",
    constr=TRUE , 
    hyper = list(theta = list(prior = "pc.prec",
                              param = c( .5 , NA ) ) # on utilise la pc.prec mais avec NA , on fixe la valeur de ec de .2
    )) +
  f(PolsenFinalFlag,model="iid",
    constr=TRUE , 
    hyper = list(theta = list(prior = "pc.prec",
                              param = c( .5 , NA ) ) # on utilise la pc.prec mais avec NA , on fixe la valeur de ec de .2
    )) +
  
  f(space, model = spde.space.Ws) +
  f(time, model = spde.time.Wt)
# +
  # f(spacetime,
  #   model = spde.residuals.Wst,
  #   group = spacetime.group, # the name is given in the fonction inla.spde.make.index
  #   control.group = list(model = "iid") )  +
  # f(
  #   mois,
  #   model = "rw1",
  #   cyclic = TRUE,
  #   constr = TRUE,
  #   scale.model = TRUE,
  #   hyper = list(theta = list(initial = log(1),
  #                             fixed = TRUE))
  # )

## 4. Fit model with INLA----

Myfit <-try( 
  inla(
    form2,
    weights = Data_zone$n ,
    family = "gaussian",
    data = inla.stack.data(mystack),
    quantiles = c(0.025, 0.05, 0.25, 0.5, 0.75, 0.95, 0.975),
    control.family = list(
      list(control.link = list(model = "identity") ,
           hyper = list(theta = list(prior = "pc.prec", param = c( .5 , .5 ) ) ) # on utilise la pc.prec mais avec NA , on fixe la valeur de ec de .2
      )),
    control.compute=list(config=OptionComputeSimul,
                         dic=TRUE,
                         waic=TRUE,
                         cpo= TRUE # cross-validated predictive measures
    ),
    control.predictor = list(compute = TRUE,
                             A = inla.stack.A(mystack)),
    control.inla = list(int.strategy = "eb",
                        strategy = "gaussian",
                        reordering = 'metis') ,
    control.results = list(
      return.marginals.random = FALSE, # Pour la validation crois�e et le calcul du CRPS
      return.marginals.predictor = FALSE # Pour la validation crois�e et le cacul du CRPS
    ),
    control.fixed = list(mean = list(
      intercept = mean(mystack$data$data$y,  na.rm = TRUE),
      default = 0
    ),
    prec = 1),
    # suggestion from Thomas
    num.threads = num.thr,
    verbose=OptionVerbose
  )##using parameter control.results we can control the output result
  ##when set as false the storage of the marginal posterior distributions of random effects and posterior predictor values is disabled .
  ##The computation of the quantiles is also disabled, by setting quantiles equal to FALSE
  
) 
