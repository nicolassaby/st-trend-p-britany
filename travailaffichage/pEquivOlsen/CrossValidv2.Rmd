---
title: "INLA results Cross valid and maps"
author: "Nicolas Saby"
date: "19/01/2022"
output:
html_document: 
    keep_md: yes
    toc: yes
    toc_float: yes
    number_sections: yes
    fig_width: 11
    fig_height: 7
    fig_caption: yes
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE,warning = FALSE,message = FALSE)

library(ggplot2)
library(foreach)
# library(tidyverse)
library(sf)
library(INLA)
library(dplyr)
library(tmap)
# library(FactoMineR)
library(dplyr)
library(DT)


MaTransform <- function(x , elt){
  if (elt == "pH")  x <- x
  if (elt == "Carbone") x <- log(x)
  if (elt == "PEquivOlsen") x <- sqrt(x)
  return(x)
}


MaBackTransform <- function(x , elt){
  if (elt == "pH")  x <- x
  if (elt == "carbone") x <- exp(x)
  if (elt == "PEquivOlsen") x <- (x)^2
  return(x)
}

source("../model/basicStats.R")

```

# Les données brutes


```{r donnees}
k <- c(3,8,15) # k+2 intervals
 
nsim <- 100  # For CRPS
 
cutoffValue <- c(0.05,0.08,0.1,0.15,.3) # Cut off of the mesh
 
elt = 'PEquivOlsen'

# elt = 'PDyer'

 elt.txt2 <- 'peqolsen'
 elt.txt <- 'PEquivOlsen'

# load(file='../../data/grillePredWithFixedeffect.RData')
xyPred <- readRDS("../../data/grillePredWithFixedeffectBret.RDS")
# load(paste0("../../data/DataBretagne",elt.txt,".RData"))
load("../../data/DataBretAcidMatParPEqOlsen.RData")

Data_zone <- Data_zone %>% 
mutate_at(vars(elt.txt2), .funs = funs(elt = MaTransform(.,elt.txt2))) %>% # transformation
mutate(eltRaw = elt ) %>% # For the transformation, save the observed value
  mutate(val = elt ) %>% # For the validation, save the observed value
  mutate(pred = NA , # pour prediction ST Xval
         pred2 = NA, # pour prediction Classical Xval
         n2 = n /max(n))

```


# Table cross validation

```{r cv, eval = TRUE}

type = c("XvalCla","XvalST","XvalSTDegrad","XvalSTDegrad2")
type = c("XvalCla","XvalST")
getwd()
r <- foreach(Ci = cutoffValue, 
             .combine = rbind.data.frame )  %:%
  
  foreach( Ki = k,
             .combine = rbind.data.frame ) %:% 
  foreach( type1 = type ,
             .combine = rbind.data.frame ) %do% {
  Monfichier = paste0(elt.txt,'BretMTOwMatParC',Ci,'S',nsim,'k',Ki)
  # file=paste0("../results/",elt.txt,"/",type1,Monfichier,'.RData')
  file=paste0("../results/","/",type1,Monfichier,'.RData')
  load(file)
 
  # resu2 <- eval(Data_zone$val,Data_zone$pred)

  if(type1 == "XvalCla") {
    resu2 <- foreach(i = 1:length(resuXval),
                       .combine = rbind.data.frame ) %do%
     resuXval[[i]]
  } else {
        resu2 <- foreach(i = 1:length(resuXval),
                           .combine = rbind.data.frame ) %do%
     resuXval[[i]]

  }
  resuM <-   colMeans(resu2)
   
 cbind.data.frame(type1,Ci,Ki,ME=resuM[1],MAE =resuM[2],r = resuM[3],r2=resuM[4],
                  NSE = resuM[5],rhoC=resuM[6],Cb=resuM[7], CRPS= resuM[8])
}


```


```{r old2, eval = FALSE}
type = c("XvalCla","XvalST","XvalSTDegrad","XvalSTDegrad2")

# r <- foreach(Ci = cutoffValue, 
#              .combine = rbind.data.frame )  %:%
#   
#   foreach( Ki = k,
#              .combine = rbind.data.frame ) %:% 
#   foreach( type1 = type ,
#              .combine = rbind.data.frame ) %do% {
#   Monfichier = paste0(elt.txt,'BretMTOwMatParC',Ci,'S',nsim,'k',Ki)
#   file=paste0("../results/",elt.txt,"/",type1,Monfichier,'.RData')
#   load(file)
#  
#   # resu2 <- eval(Data_zone$val,Data_zone$pred)
# 
#   if(type1 == "XvalCla") {
#     resu2 <- foreach(i = 1:length(resuXval),
#                        .combine = rbind.data.frame ) %do%
#      resuXval[[i]]
#   } else {
#         resu2 <- foreach(i = 1:length(resuXval),
#                            .combine = rbind.data.frame ) %do%
#      resuXval[[i]]
# 
#   }
#   resuM <-   colMeans(resu2)
#    
#  cbind.data.frame(type1,Ci,Ki,ME=resuM[1],MAE =resuM[2],r = resuM[3],r2=resuM[4],
#                   NSE = resuM[5],rhoC=resuM[6],Cb=resuM[7], CRPS= resuM[8])
# }
# 

```


```{r}
library(ggplot2)
library(dplyr)
library(tidyr)

# r %>% pivot_longer(!type1:Ki, names_to = "indicateur", values_to = "valeur") %>%
# ggplot(aes(factor(Ci),valeur,col=factor(Ki),shape=factor(Ki) ))+ 
#   geom_point() + facet_grid(indicateur~type1,scales = "free_y")

r %>% gather("indicateur","valeur",-(type1:Ki) ) %>%
ggplot(aes(factor(Ci),valeur,col=factor(Ki),shape=factor(Ki) ))+ 
  geom_point() + facet_grid(indicateur~type1,scales = "free_y")


```

## dans l'espace et le temps

la répartition des observations

```{r carte}
bdat_canton <- Data_zone %>% 
  mutate(periode = cut(annee, breaks = c(1990,1995,1999,2004,2009,2014,2019),
                       labels = c('1990-1994','1995-1999','2000-2004','2005-2009','2010-2014','2015-2019')
  )) %>%
  filter(!is.na(periode)) %>%
  # filter(periode %in% c('P3','P4')) %>%
  
  group_by(x,y,periode) %>%
  summarise(med = median(elt),
            
            n= length(elt)) %>%
  st_as_sf( coords = c("x", "y"), crs = 2154, agr = "constant")



cut<- quantile(bdat_canton$med,prob=c(0,.1,.3,.4,.5,.7,.9,1),na.rm = T )
tm_shape(bdat_canton) +
  tm_dots("med",size = .15,
          breaks = cut,
              palette = "RdYlGn", 
              border.col = "grey", 
              border.alpha = 1,midpoint = NA)+
  # tm_layout(basemaps = leaflet::providers$OpenStreetMap)+

  tm_facets(by='periode')

```


```{r carte2}
bdat_canton <- Data_zone %>% 
  mutate(periode2 = cut(annee, breaks = seq(from = 1990,to = 2020, by = 2)
  )) %>%
  filter(!is.na(periode2)) %>%
  # filter(periode %in% c('P3','P4')) %>%
  
  group_by(x,y,periode2) %>%
  summarise(med = median(elt),
            
            n= length(elt)) %>%
  st_as_sf( coords = c("x", "y"), crs = 2154, agr = "constant")



cut<- quantile(bdat_canton$med,prob=c(0,.1,.3,.4,.5,.7,.9,1),na.rm = T )
tm_shape(bdat_canton) +
  tm_dots("med",size = .35,
          breaks = cut,
              palette = "RdYlGn", 
              border.col = "grey", 
              border.alpha = 1,midpoint = NA)+
  # tm_layout(basemaps = leaflet::providers$OpenStreetMap)+

  tm_facets(by='periode2')

```

## dans le temps uniquement

Et enfin une reprsentation dans le temps de la variabilité des données

```{r}
Data_zone %>% 
  mutate(periode = cut(annee,
                       breaks = 1990:2020,
                       labels= 1991:2020
  )) %>%
  filter(!is.na(periode)) %>%
  # filter(periode %in% c('P3','P4')) %>%
  
  group_by(x,y,periode) %>%
  summarise(med = median(elt),
            
            n= length(elt)) %>%
  ggplot(aes(periode,med)) + geom_boxplot()
  

Data_zone %>% 
  mutate(periode = as.numeric(
           as.character(cut(annee,
                       breaks = 1990:2020,
                       labels= 1991:2020
  ) ) )
  ) %>%
  dplyr::filter(!is.na(periode)&!is.na(elt)) %>%
  # filter(periode %in% c('P3','P4')) %>%
  
  group_by(periode) %>%
  summarise(med = median(elt),
            q99 = quantile(elt,prob=.99),
            q90 = quantile(elt,prob=.90),
            q10 = quantile(elt,prob=.10),
            q01 = quantile(elt,prob=.01),
            n= length(elt)) %>%
  dplyr::select(-n) %>%
  dplyr::filter(periode> 1995) %>%
  gather(key,value,-periode) %>%
  ggplot(aes(as.numeric(periode),value,col=key)) + 
  geom_line()+
  geom_smooth()

```

# Le modèle finale

## Choix des hyper paramètres
```{r}
MonfichierList <- readRDS(paste0("../results/","/","ModelFinal",elt.txt,'.RData'))

Monfichier <- MonfichierList[[1]]
k = MonfichierList[[2]][1]

load(paste0("../results/","/","Fit",Monfichier,'.RData') )
load(paste0("../results/","/","Simul",Monfichier,'.RData') )

```

```{r}

elt <- elt.txt
# Define the temporal mesh
tknots=1:k/(k+1)-0.5

#mesh.time=inla.mesh.1d(tknots,interval=c(0,1),boundary="dirichlet")
mesh.time=inla.mesh.1d(tknots,
                       interval=c(-0.5,0.5),
                       boundary="free",
                       free.clamped=TRUE)

```


## la triangulation

Voici le mesh choisi

```{r}
xyMesh=as.matrix(Data_zone[,c("x","y")])##build coordinate matrix
xyMesh<-xyMesh/100000 # for INLa keep X and Y small to avoid any numerical instabilty
plot(mesh)
# points(xyMesh,col='red',pch='+')

```


Finalement le modèle utilisé

\begin{equation}\label{eq:mod-notrend}
    Y(s,t)=\beta_0+\sum_{i\in \text{scorpan}}\beta^{\text{sc}}_i z^{\text{sc}}_i(s)+f(\text{month}(t))+W_0(s)+(\tilde{t}-0.5)W_1(s)+\varepsilon(s,t),
    \end{equation}

ici pas de $f(\text{month}(t))$

# La validation croisée 

```{r, eval = FALSE}
file=paste0("../results/",elt,"/","XvalST",Monfichier,'.RData')
getwd()
load(file)
resu1 <- matrix((unlist(resuXval2)),ncol=6,byrow = TRUE)

file=paste0("../results/",elt,"/","XvalCla",Monfichier,'.RData')
load(file)
resu2 <- matrix((unlist(resuXvalClassique)),ncol=6,byrow = TRUE)


load(paste0("../results/",elt,"/","XvalSTDegrad",elt,'BretMTOw','C',cutoffValue,'S',nsim,'k',k,'.RData'))
resu3 <- matrix((unlist(resuXval2)),ncol=6,byrow = TRUE)
#
load(paste0("../results/",elt,"/","XvalClaDegrad",elt,'BretMTOw','C',cutoffValue,'S',nsim,'k',k,'.RData'))
resu4 <- matrix((unlist(resuXvalClassique)),ncol=6,byrow = TRUE)

tt <- cbind.data.frame(
  meth = c("XvalST","XvalCla","XvalSTDegrad","XvalClaDegrad"),
  round( rbind(colMeans(resu1),colMeans(resu2),colMeans(resu3),colMeans(resu4)) , 4)
)

colnames(tt)[-1] <- c("R²","Biais","RMSE","indi1","indic2","CRPS")

datatable(tt )

```
# Les sorties brutes


## Les hyper paramètres

### les effets spatio temporels

$W_0(s)$ et $W_1(s)$

```{r}
j=7
tmp = Myfit$marginals.hyper[[j]]
plot(tmp[ , 1]*100 ,
     tmp[ , 2] ,
     type='l', 
     xlab=paste(names(Myfit$marginals.hyper)[j],' in km'),
     ylab='Density',
     xlim=c(0,1000)
)

j=which(names(Myfit$marginals.hyper)=='Range for space')
tmp = Myfit$marginals.hyper[[j]]
plot(tmp[ , 1]*100 ,
     tmp[ , 2] ,
     type='l', 
     xlab=paste(names(Myfit$marginals.hyper)[j],' in km'),
     ylab='Density',
     xlim=c(0,1000)
)

```

### Les effet "fixes"

$$\sum_{i\in \text{scorpan}}\beta^{\text{sc}}_i$$


```{r}
Mytable <- Myfit$summary.random$matcl

ggplot(Mytable,aes(factor(ID),mean)) +
  geom_point() +
  geom_point(aes(factor(ID),`0.025quant`),col='grey') +
  geom_point(aes(factor(ID),`0.975quant`),col='grey') +
  geom_abline(slope = 0,intercept = 0)+
  xlab('Parental Material')+ylab('Coefficient (in log)')+
  theme_bw()+ theme(text = element_text(size=20))

# Mytable <- Myfit$summary.random$mois
# 
# ggplot(Mytable,aes(factor(ID),mean)) +
#   geom_point() +
#   geom_point(aes(factor(ID),`0.025quant`),col='grey') +
#   geom_point(aes(factor(ID),`0.975quant`),col='grey') +
#   geom_abline(slope = 0,intercept = 0)+
#   xlab('Parental Material')+ylab('Coefficient (in log)')+
#   theme_bw()+ theme(text = element_text(size=20))


```

```{r}
Mytable <- Myfit$summary.random$FIRST_ENZ

ggplot(Mytable,aes(factor(ID),mean)) +
  geom_point() +
  geom_point(aes(factor(ID),`0.025quant`),col='grey') +
  geom_point(aes(factor(ID),`0.975quant`),col='grey') +
  geom_abline(slope = 0,intercept = 0)+
  xlab('Climate')+ylab('Coefficient (in log)')+
  theme_bw()+ theme(text = element_text(size=20))
```

## les cartographies
### l'évolution $W_1(s)$

La tendance temporelle $W_1(s)$, ici on cartographie la médiane de la posterieur.

```{r}
nsample=dim(sim.components[[3]])[2] 

# Map the simulations ------------

predGid <- xyPred %>% filter(CODE_DEPT %in% c('22','29','35','56'))

grid.proj=inla.mesh.projector(mesh, as.matrix(predGid[,1:2]) )
grid.proj.st=inla.mesh.projector(mesh.st, as.matrix(predGid[,1:2]) )



# CArtographie de la tendance générale ------
predGid$timeM=inla.mesh.project(grid.proj,rowMeans(sim.components$time) )
predGid$timeM=inla.mesh.project(grid.proj,Myfit$summary.random$time$mean )

ggplot(predGid,aes(x,y,fill=timeM)) +
  geom_tile() + 
  # scale_fill_gradientn(colours=c("#0000FFFF","#FFFFFFFF","#FF0000FF"))+
  scale_fill_gradient2(low="#FF0000FF", mid="white", high="navy", 
                       midpoint=0, limits=range(predGid$timeM)) +
  labs(fill = "Time trend Q50")+
  
  coord_equal()

```

on peut cartographier l'incertitude associée en utilisant les simulations

```{r}
predGid$timeLL <- inla.mesh.project(grid.proj,Myfit$summary.random$time$`0.025quant`)
predGid$timeUL <- inla.mesh.project(grid.proj,Myfit$summary.random$time$`0.975quant`)

predGid %>%
  dplyr::select(x,y,timeM,timeLL,timeUL) %>%
  gather("key","value",-x,-y) %>%
  ggplot(aes(x,y,fill=value)) +
  geom_tile() + 
  # scale_fill_gradientn(colours=c("#0000FFFF","#FFFFFFFF","#FF0000FF"))+
  scale_fill_gradient2(low="#FF0000FF", mid="#00FF66FF", high="#CC00FFFF",  
                       midpoint=0, limits=range(predGid$timeLL,predGid$timeUL)) +
  labs(fill = "Spatial trend ")+
  facet_wrap(~key,nrow = 3)+
  coord_equal()+
  theme_void()



```


Il est donc possible d'inférer si la valeur 0 est incluse dans l'intervalle de crédibilité ou dans l'intervalle calculé par les simulations

```{r}
predGid$Sig <- ifelse(predGid$timeM>0,
                      ifelse(predGid$timeLL>0,'Positive Trend','Not Sign'),
                      ifelse(predGid$timeUL<0,'Negative Trend','Not Sign'))

ggplot(predGid,aes(x,y,fill=factor(Sig) )) +
  scale_fill_brewer(palette = "RdBu") +
  geom_raster() + 
  labs(fill = "Signif. Time trend")+
  
  # scale_fill_gradientn(colours=c("#0000FFFF","#FFFFFFFF","#FF0000FF"))+
  coord_equal()

```
```{r}
predGid$matcl2 <- predGid$matcl

predGid$effmat <- Myfit$summary.random$matcl$`0.5quant`[predGid$matcl2]

ggplot(predGid,aes(x,y,fill=(effmat) )) +
  scale_fill_gradient2(low="#FF0000FF", mid="#00FF66FF", high="#CC00FFFF",  
                       midpoint=0, limits=range(-0.21,0.2)) +
  geom_raster() + 
  labs(fill = "mat par")+
  
  # scale_fill_gradientn(colours=c("#0000FFFF","#FFFFFFFF","#FF0000FF"))+
  coord_equal()

```

### La tendance globale $W_0(s)$

on ajoute le $W_0(s)$ et l'intercept $\beta_0$
De la même manière, on peut carctériser l'incertitude

```{r}
valuetoMap = ( 
  t( sim.components$intercept[1,] + t(sim.components$space) )
  )

predGid$SpaM=inla.mesh.project(grid.proj, rowMeans(valuetoMap) ) 

predGid$SpaLL <- inla.mesh.project(grid.proj,
                                    apply(valuetoMap ,
                                          1,
                                          quantile,
                                          probs=c(0.1),na.rm=TRUE))
predGid$SpaUL <- inla.mesh.project(grid.proj,
                                    apply(valuetoMap ,
                                          1,
                                          quantile,
                                          probs=c(0.9),na.rm=TRUE))
predGid %>%
  dplyr::select(x,y,SpaM,SpaLL,SpaUL) %>%
  gather("key","value",-x,-y) %>%
  ggplot(aes(x,y,fill=value)) +
  geom_tile() + 
  # scale_fill_gradientn(colours=c("#0000FFFF","#FFFFFFFF","#FF0000FF"))+
  scale_fill_gradient2(low="#FF0000FF", mid="#00FF66FF", high="#CC00FFFF",  
                       midpoint=median(predGid$SpaM), limits=range(predGid$SpaUL,predGid$SpaLL)) +
  labs(fill = "Spatial trend ")+
  facet_wrap(~key,nrow = 3)+
  coord_equal()+
  theme_void()

```


# les prédictions annuelles

Il faut préparer la grille pour la rendre compatible avec inla qui ne gère pas bien les facteurs R

```{r, include=TRUE}
table(predGid$FIRST_ENZ)
predGid$FIRST_ENZ2 <- ifelse(predGid$FIRST_ENZ==3,1,ifelse(predGid$FIRST_ENZ==5,2,NA) )
table(predGid$FIRST_ENZ2)

table(predGid$matparBret)
 predGid$matcl2 <- predGid$matcl
# predGid$matcl2 <- ifelse(predGid$matcl==4,3,predGid$matcl2 )
# predGid$matcl2 <- ifelse(predGid$matcl==5,4,predGid$matcl2 )
# predGid$matcl2 <- ifelse(predGid$matcl==7,5,predGid$matcl2 )
# predGid$matcl2 <- ifelse(predGid$matcl==9,6,predGid$matcl2 )

predGid$matcl2 <- predGid$matparBret
grid.proj=inla.mesh.projector(mesh, as.matrix(predGid[,1:2]) )
grid.proj.st=inla.mesh.projector(mesh.st, as.matrix(predGid[,1:2]) )

```

Il faut ensuite produire les prédictions pour chaque $k$ en chaque pixels $s$

Pour cela, il faut également résumé les simulations de chaque effet

```{r}
library(foreach)
MinA = min(Data_zone$annee)
MaxA = max(Data_zone$annee)

nbtimesot = MaxA-MinA +1

sim=1 # for debugging

resu <-
  foreach(
    t.target = seq( from = -0.5,  to = 0.5, length.out = nbtimesot) ,
    .combine = cbind.data.frame) %do% {
    
    
    predAnnee = foreach(sim =  1:nsample ,
                      .combine = cbind.data.frame) %do% {
                        # intercept
                        b0 <- sim.components$intercept[sim]
                        
                        #Space
                        valuesOnMesh = sim.components$space[, sim]
                        
                        field2plot = b0 + # intercept
                          sim.components$FIRST_ENZ[, sim][predGid$FIRST_ENZ2] + # climate effect
                          sim.components$matcl[, sim][predGid$matcl2] + # matpar  effect
                          inla.mesh.project(grid.proj, field = valuesOnMesh) # space random effect
                        
                        valuesOnMesh = t.target * sim.components$time[, sim] # time random effect (trend)
                        
                        field2plot = field2plot +
                          inla.mesh.project(grid.proj, field = valuesOnMesh)
                        
                        # space time residuals
                        # compute the weights of the b-splines
                        weights = as.numeric(inla.spde.make.A(mesh.time, 
                                                              loc = t.target))
                        idx.res = which(weights > 0)
                        
                        for (i in 1:length(idx.res)) {
                          valuesOnMesh = weights[idx.res[i]] *
                            sim.components$spacetime[ (idx.res[i] - 1) * mesh.st$n + 1:mesh.st$n, 
                                                      sim ]
                          
                          field2plot = field2plot + 
                            inla.mesh.project(grid.proj.st,
                                              field = valuesOnMesh)
                        }
                        
                        # backtransformation if needed
                        
                        MaBackTransform (field2plot,elt.txt)
                      }
    
    rowMeans((predAnnee))
  }



resuLL <-   foreach(t.target = seq( from = -0.5,  to = 0.5, length.out = nbtimesot) ,
                    .combine = cbind.data.frame) %do% {
                      
                      
                      predAnnee = foreach(sim =  1:nsample ,
                                          .combine = cbind.data.frame) %do% {
                                            # intercept
                                            b0 <- sim.components$intercept[sim]
                                            
                                            #Space
                                            valuesOnMesh = sim.components$space[, sim]
                                            
                                            field2plot = b0 + # intercept
                                              sim.components$FIRST_ENZ[, sim][predGid$FIRST_ENZ2] + # climate effect
                                              sim.components$matcl[, sim][predGid$matcl2] + # climate effect
                                              inla.mesh.project(grid.proj, field = valuesOnMesh) # space random effect
                                            
                                            valuesOnMesh = t.target * sim.components$time[, sim] # time random effect (trend)
                                            
                                            field2plot = field2plot +
                                              inla.mesh.project(grid.proj, field = valuesOnMesh)
                                            
                                            # space time residuals
                                            # compute the weights of the b-splines
                                            weights = as.numeric(inla.spde.make.A(mesh.time, loc =
                                                                                    t.target))
                                            idx.res = which(weights > 0)
                                            
                                            for (i in 1:length(idx.res)) {
                                              valuesOnMesh = weights[idx.res[i]] *
                                                sim.components$spacetime[(idx.res[i] -
                                                                                       1) * mesh.st$n + 1:mesh.st$n,
                                                                         sim]
                                              
                                              field2plot = field2plot + inla.mesh.project(grid.proj.st, field =
                                                                                            valuesOnMesh)
                                            }
                                            
                                            # field2plot^2 # backtransformation if needed
                                            MaBackTransform (field2plot,elt.txt)

                                          }
                      
                      apply( (predAnnee),
                            1,
                            quantile,
                            probs=c(0.05),
                            na.rm=TRUE)
                    }



  
resuUL <-   foreach(t.target = seq( from = -0.5,  to = 0.5, length.out = nbtimesot) ,
                    .combine = cbind.data.frame) %do% {
                      
                      
                      predAnnee = foreach(sim =  1:nsample ,
                                          .combine = cbind.data.frame) %do% {
                                            # intercept
                                            b0 <- sim.components$intercept[sim]
                                            
                                            #Space
                                            valuesOnMesh = sim.components$space[, sim]
                                            
                                            field2plot = b0 + # intercept
                                              sim.components$FIRST_ENZ[, sim][predGid$FIRST_ENZ2] + # climate effect
                                              sim.components$matcl[, sim][predGid$matcl2] + # climate effect
                                              inla.mesh.project(grid.proj, field = valuesOnMesh) # space random effect
                                            
                                            valuesOnMesh = t.target * sim.components$time[, sim] # time random effect (trend)
                                            
                                            field2plot = field2plot +
                                              inla.mesh.project(grid.proj, field = valuesOnMesh)
                                            
                                            # space time residuals
                                            # compute the weights of the b-splines
                                            weights = as.numeric(inla.spde.make.A(mesh.time, loc =
                                                                                    t.target))
                                            idx.res = which(weights > 0)
                                            
                                            for (i in 1:length(idx.res)) {
                                              valuesOnMesh = weights[idx.res[i]] *
                                                sim.components$spacetime[(idx.res[i] -
                                                                                       1) * mesh.st$n + 1:mesh.st$n,sim]
                                              
                                              field2plot = field2plot + inla.mesh.project(grid.proj.st, field =
                                                                                            valuesOnMesh)
                                            }
                                            
                                            # field2plot^2 # backtransformation if needed
                        MaBackTransform (field2plot,elt.txt)
                                          }
                      
                      apply( (predAnnee),
                            1,
                            quantile,
                            probs=c(0.95),
                            na.rm=TRUE)
                    }



colnames(resu) <- paste('Year',MinA:MaxA,sep='.')
colnames(resuLL) <- paste('LLYear',MinA:MaxA,sep='.')
colnames(resuUL) <- paste('ULYear',MinA:MaxA,sep='.')

predGidSp <- cbind.data.frame(predGid,resu)
predGidSp <- cbind.data.frame(predGidSp,resuLL,resuUL)

```

On peut ensuite produire une carte pour un $t$ choisi, ici $t =  2014$

```{r}
predGidSp %>%
  dplyr::select(x,y,Year.2004,LLYear.2004,ULYear.2004) %>%
  gather("key","value",-x,-y) %>%
  ggplot(aes(x,y,fill=value)) +
  geom_tile() + 
  # scale_fill_gradientn(colours=c("#0000FFFF","#FFFFFFFF","#FF0000FF"))+
  scale_fill_gradient2(low="#FF0000FF", mid="#00FF66FF", 
                       high="#CC00FFFF",  
                       midpoint=median(predGidSp$Year.2014, na.rm=TRUE),
                       limits=range(predGidSp$LLYear.2014,predGidSp$ULYear.2014, na.rm=TRUE)) +
  labs(fill = "Prediction pour 2014")+
  facet_wrap(~key,nrow = 3)+
  coord_equal()+
  theme_void()

```

Avec le package tmap

```{r}
library(tmap)
br = quantile( c(predGidSp$Year.2000,predGidSp$ULYear.2000,predGidSp$LLYear.2000),
               prob= c(0,.1,.2,.3,.4,.5,.6,.7,.8,.9,1),na.rm=TRUE)

predGidSp2 <- predGidSp
coordinates(predGidSp2) <- c('x','y')
gridded(predGidSp2) = TRUE
# proj4string(predGidSp2) <- CRS("+init=epsg:27572")
proj4string(predGidSp2) <- CRS("+proj=lcc +lat_1=46.8 +lat_0=46.8 +lon_0=0 +k_0=0.99987742 +x_0=600000 +y_0=2200000 +ellps=clrk80ign +pm=paris +units=m +no_defs")

# library(raster)
# x <- raster(predGidSp2,crs="27572")
# merc <- projectRaster(x, crs = "+init=epsg:3857")


m <- tm_shape(predGidSp2,projection = 27572) + 
  tm_raster(col= 'Year.2014',
            breaks = br,
            # style = "quantile", n = 12, 
            title = "Q50 Predicted C content",
            palette = colorRampPalette( c("darkolivegreen4","yellow", "brown"))(10),
            legend.hist = TRUE) +
  tm_legend(outside = TRUE, hist.width = 2)

ll <- tm_shape(predGidSp2) + 
  tm_raster(col= 'LLYear.2014',
            breaks = br,
            # style = "quantile", n = 12, 
            title = "Q05 Predicted C content",
            palette = colorRampPalette( c("darkolivegreen4","yellow", "brown"))(10),
            legend.hist = TRUE)+
  tm_legend(outside = TRUE, hist.width = 2)

ul <- tm_shape(predGidSp2) + 
  tm_raster(col= 'ULYear.2014',
            breaks = br,
            # style = "quantile", n = 12, 
            title = "Q95 Predicted C content",
            palette = colorRampPalette( c("darkolivegreen4","yellow", "brown"))(10),
            legend.hist = TRUE)+
  tm_legend(outside = TRUE, hist.width = 2)

tmap_arrange(ll,m,ul)

```


## representation pour certain pixel

Les trajectoires temporelles pour une sélection aléatoire de pixels

```{r}

resu %>% 
  filter(!is.na(Year.1996) )%>%
  slice_sample(n=200) %>%
  mutate(id = row_number()) %>%
  gather(annee,value,-id) %>%
  ggplot(aes(annee,value,group=id)) + 
  geom_line(alpha=.35)
  

```


# Animation

construction du raster pour l'animation


```{r}
library(raster)

s <- raster(predGidSp2[,22])
            

id <- grep("^Year" , colnames(predGidSp2@data))

for (i in id) s <- stack(s,raster(predGidSp2[,i]))

tm <- tm_shape(s) +
  tm_raster(style = "quantile", n = 10)+
  tm_facets( nrow = 1, ncol = 1,
             free.coords = FALSE) + 
    tm_legend(outside = TRUE, hist.width = 2)

tmap_animation(tm, "predP.gif", loop = TRUE, delay = 80)
```

Une petite animation présentant l'évolution par année sur la période


```{r}
magick::image_read("predP.gif")
```

