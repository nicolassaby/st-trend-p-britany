![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

# ST trend P Britany using INLA SPDE

## objet

Statistical analysis using INLA SPDE of the spatio temporal trends in soil properties in Britany

ce répertoire contient les scripts de modélisation avec INLA couplé à SPDE pour les propriétés mesurées sur des parcelles agricoles et collectées dans le cadre du projet  BDAT,
Carbone, phosphre et pH

Ce travail a été effectué dans le cadre d'un pari scientifique financé par le département **Agroécosystème** en collaboration avec Thomas Opitz et Denis Allard de BioSp

pour accélérer les calculs, il est possible d'installer une licence pardiso en allant sur le site :https://pardiso-project.org/r-inla/. Cependant, la licence pour la recherche n'est pour l'instant plus distribuée (01/2022)

## scripts

le modèle est stocké dans le répertoire modèle
l'exploitation des résultats est stocké sous forme de script Rmarkdown dans les répertoires portant le nom de la propriété.

## Résultats

Des résultats sont accessibles sur la page suivante: 

[résultats] http://nicolassaby.pages.mia.inra.fr/st-trend-p-britany/


This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

## element du modèle

- Effet d'échantillonnage préférentiel, on utilise le `nlogCl`. l'idée est de tester si 0 fait partie de l'indice de crédibilité.
- Effet du mois de prélèvement 

