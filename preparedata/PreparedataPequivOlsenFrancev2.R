# 1 Packages-------------------------------------------------------
library(INLA)
library(sf)
library(RODBC)
library(dplyr)
library(tidyverse)
library(xlsx)
library(foreign)
library(sp)
library(stringr)


# 2 collect data-------------------------------------------------------
# Collect BDAT

connection  <- odbcConnect("BDAT")

RequeteBDAT <- "
SELECT
A.bdatid,
A.annee,
A.mois,
A.insee,
pho,
p2d,
p2o,
B.peqolsen as PEquivOlsen,
CASE WHEN p2j < 0 THEN -0.5 * p2j ELSE p2j END as p2j,
x_commune  as x,y_commune as y
FROM
data.bdat_com as A inner join data.bdat_com_dela as B using(bdatid)
where A.annee > 1989 and A.annee < 2021
and pho > 4 and pho< 11 and 
(p2o is not null OR p2j is not null OR p2d is not null)
and A.depart not in ('75','92','93','2A','2B')
"
DataBDAT <- sqlQuery(connection, RequeteBDAT,as.is = FALSE)

odbcCloseAll()


DataBDAT$bdatid <- 1:nrow(DataBDAT)
DataBDAT$insee <- factor(str_pad(DataBDAT$insee,5,sid='left','0'))



table(DataBDAT$annee)
summary(DataBDAT)

# Collect the communes limits
communes <- read_sf('D:/unsavensabyD/Pari/7/Pari Scientifique - Do~1/data/admin/COMMUNE.shp') %>% st_set_crs(  2154) 

## The ptfs:----------

# P Olsen = 130.657 + 0.829 P Dyer - 0.195 pHwater
# P Olsen = 299.664 + 0.270 P Joret-Hébert - 35.208 pHwater

# one constraints is to estimate P olsen within the classical interval
# (p2j <= 500 AND p2d <= 1000 AND p2o <= 250)

DataBDAT2 <- DataBDAT %>%
  dplyr::filter(p2j <= 500 | is.na(p2j) ) %>%
  dplyr::filter(p2d <= 1000 | is.na(p2d)) %>%
  dplyr::filter(p2o <= 250| is.na(p2o)) %>%
  mutate(p2j = ifelse(p2j<0,-0.5*p2j,p2j)) %>%
  
  # mutate(P_pred = ifelse(!is.na(p2d), 130.657 + 0.829 * p2d - 0.195 * pho, NA) ) %>%
  # mutate(P_pred = ifelse(!is.na(p2j), 299.664 + 0.270 * p2j - 35.208 * pho, P_pred) ) %>%
  # mutate(P_pred = ifelse(!is.na(p2o), p2o, P_pred) ) %>%
  # 
  filter(pequivolsen>0)

DataBDAT2 <- DataBDAT2 %>% 
  mutate(PolsenFinalFlag = ifelse(!is.na(p2d), 'p2d', NA) ) %>%
  mutate(PolsenFinalFlag = ifelse(!is.na(p2o), 'p2o', PolsenFinalFlag) ) %>%
  mutate(PolsenFinalFlag = ifelse(!is.na(p2j), 'p2j', PolsenFinalFlag) )

table(DataBDAT2$PolsenFinalFlag)


DataBDAT2 <- DataBDAT2 %>%
  dplyr::filter(  !is.na(insee)  ) %>% # eliminate a lot of data
  filter(!is.na(x))


summary(DataBDAT2)

table(DataBDAT2$annee)
summary(DataBDAT2)
hist(DataBDAT2$pequivolsen,breaks = 150)

ggplot(DataBDAT2 , aes(x= pequivolsen,fill=PolsenFinalFlag)) + geom_histogram()
ggplot(DataBDAT2 , aes(x= p2d)) + geom_histogram()


resu1 <- DataBDAT2 %>% 
  st_as_sf(coords = c("x", "y"),
           crs = 2154, agr = "constant") %>%
  st_join(communes) %>%
  dplyr::select(bdatid,INSEE_COM) %>%
  st_set_geometry(NULL)

DataBDAT2 <- DataBDAT2 %>%
  right_join(resu1 ,
             by = c('bdatid')) %>%
  filter(!is.na(INSEE_COM)) 

##Extract code for department
DataBDAT2$depart <- substr(DataBDAT2$INSEE_COM,1,2)

as.data.frame(table(DataBDAT2$depart))


## delete islands

DataBDAT2 <- DataBDAT2 %>%
  filter(
    !insee %in% c("56241","56009","56114","56186","56234","56086",
                  "56152","56082","56085","56069","29155")
  )

as.data.frame(table(DataBDAT2$depart))

# 4 Compute fixed effects --------------------------------------------------------
# Number of data ---------
NbAna <- DataBDAT2 %>% 
  group_by(INSEE_COM) %>%
  summarise(n = n()) %>%
  mutate(n2 = n /max(n),# to normalized the number of data per commune
         nlog= log(n)) # we prefer to keep  n , then all values are positive

DataBDAT2 <- DataBDAT2 %>%
  left_join(NbAna,by = c('INSEE_COM'='INSEE_COM'))

hist(NbAna$n,breaks = 150)
boxplot(NbAna$n,breaks = 150)

N = 1800 # for selecting commune with a lot of data

id <- which(NbAna$n>N)

DataBDAT2 %>%
  filter(INSEE_COM %in% NbAna$INSEE_COM[id]) %>%
  ggplot(aes(INSEE_COM,pequivolsen ) ) +
  geom_boxplot() +
  coord_flip()

# Parental Material ----
smucom <- read.csv2('D:/unsavensabyD/Pari/7/Pari Scientifique - Do~1/data/bdat/com2012-SMU-allfrance.csv') %>%
  rename(insee = ins_cmm.C.80 ,  SMU = SMU.N.10.0 , AREA  = AREA.N.19.11,
         PERCENTAGE = PERCENTAGE.N.19.11) %>%
  group_by(insee,SMU) %>%
  summarise(pcSMU = max(PERCENTAGE)) %>%
  ungroup()

library(foreign)
stu <- read.dbf('D:/database/pedo/france/stu.dbf')
stu.org <- read.dbf('D:/database/pedo/france/stuorg.dbf')

# Reclassified the parental material in 8 cl
stu$matcl <- as.character(stu$MAT11)
stu$matcl[stu$MAT12 %in% c('33','35')] <- '2' #Calcareous clay, Residual clay from calcareous rocks
stu$matcl <- factor(stu$matcl)

smuMAT <- stu.org %>% 
  left_join( select(stu,STU,matcl),
             by = 'STU') %>%  
  # left_join(mat,by =c('MAT1' ='mat') ) %>% 
  
  select(SMU,STU,PCAREA,matcl) %>% na.omit() %>%
  select(-STU) %>% group_by(SMU , matcl) %>%
  summarise_each(  funs(sum) , PCAREA ) %>% 
  mutate(the_rank  = rank(-PCAREA, ties.method = "random")) %>%
  filter(the_rank == 1) %>% select(-the_rank) %>%
  as.data.frame()

comMAt <- smucom %>% 
  left_join(smuMAT, by='SMU') %>%
  mutate(pcfinal = (pcSMU * (PCAREA / 100) ) )%>%
  select(insee,pcfinal,matcl) %>% na.omit()  %>% 
  group_by(insee , matcl) %>%
  summarise_each(  funs(sum) , pcfinal ) %>% 
  mutate(the_rank  = rank(-pcfinal, ties.method = "random")) %>%
  filter(the_rank == 1) %>% select(-the_rank) %>%
  as.data.frame() 



DataBDAT2 <- DataBDAT2 %>%
  left_join(comMAt,by = c('insee'='insee'))



# Seamzones / Climate --------
library(lwgeom)

climate <- read_sf('R:/Landmark/WP/WP2/Data/JRC/Seamzones/shapes_Liliane/seamzones_environmentalzone.shp') %>%
  st_transform( crs = 2154) %>% st_make_valid()

# Too long : climate <-  climate %>% st_intersection(communes)

climateJ <-  st_join(
  st_as_sf(dplyr::select(DataBDAT2,bdatid,x,y) ,
           coords = c("x", "y"), crs = 2154, agr = "constant"),
  climate
)

DataBDAT2 <- DataBDAT2 %>%
  left_join(dplyr::select(climateJ,bdatid,FIRST_ENZ) ,by = c('bdatid'='bdatid'))

DataBDAT2$FIRST_ENZ <- factor(DataBDAT2$FIRST_ENZ)

DataBDAT2$nlogCl <- cut(DataBDAT2$nlog,
                        breaks = quantile(DataBDAT2$nlog,
                                          prob = c(0,.25,.5,.75,1) ), 
                        include.lowest = TRUE  )

DataBDAT2$PEquivOlsen <- DataBDAT2$pequivolsen

saveRDS(DataBDAT2,file='data/DataFRAAcidMatParPEquivOlsenRaw.RDS') 

# Perform a selection of a subset of data -----------


Data_zone<-DataBDAT2 %>%
  filter(
    ! depart %in%  c('2A','2B') &
      !is.na(matcl) &
      !is.na(FIRST_ENZ) &
      !is.na(nlogCl) &
      n<N &
      !is.na(pequivolsen) 
    # & annee > 1994
  ) %>%
  group_by(x,y,annee,matcl,FIRST_ENZ,nlogCl,PolsenFinalFlag) %>% 
  summarise(PEquivOlsen= mean(pequivolsen),
            
            n=n()) %>% 
  ungroup() %>%
  as.data.frame()

# sample_frac(.4) %>%


ggplot(Data_zone[,],aes(as.factor(matcl) , PEquivOlsen) ) + geom_boxplot() 
ggplot(Data_zone[,],aes(FIRST_ENZ , PEquivOlsen) ) + geom_boxplot() 
ggplot(Data_zone[,],aes(nlogCl , PEquivOlsen) ) + geom_boxplot() 
ggplot(Data_zone[,],aes(PolsenFinalFlag , PEquivOlsen) ) + geom_boxplot() 
hist(Data_zone$PEquivOlsen,breaks=30)

# Data_zone %>%
#   left_join(NbAna,by = c('INSEE_COM'='INSEE_COM')) %>%
#   dplyr::filter(INSEE_COM %in% NbAna$INSEE_COM[id]) %>%
#   ggplot(aes(INSEE_COM,carbone) ) + 
#   geom_boxplot() +
#   coord_flip()

## Implement the cross validation ----------

# ST cross validation
set.seed(123098)

size = Data_zone %>% distinct(x,y) %>% dim 

xy <- Data_zone %>% distinct(x,y) %>%
  mutate(fold = sample(1:5,size=size[1], replace = TRUE))

ggplot(xy[1:50,],aes(x,y,col=factor(fold)  ))  + geom_point()

Data_zone <- Data_zone %>% left_join(xy, by=c('x','y'))

# Classical Xvalication
Data_zone <- Data_zone %>% 
  mutate(fold2 = sample(1:5, size=dim(Data_zone)[1], replace = TRUE))

# size = Data_zone %>% distinct(x,y,annee) %>% dim 
# xya <- Data_zone %>% 
#   distinct(x,y,annee) %>%
#   mutate(fold3 = sample(1:5,
#                         size=size[1],
#                         replace = TRUE)
#          )
# 
# ggplot(xya %>% filter(x==134700   & y==6853800),aes(x,y,col=factor(fold3)  ))  +
#   geom_jitter(width = 5, height = 5)
# 
# xya %>% filter(x==134700   & y==6853800) %>% arrange(annee)
# 
# xya %>% filter(annee==2001 & x==134700   & y==6853800)
# 
# Data_zone %>% group_by(x,y,annee) %>% summarise(n = n())%>% filter(n>1)
# 
# Data_zone <- Data_zone %>% left_join(xya, by=c('x','y','annee'))


# Prepare mesh
# lim <- read_sf('D:/database/habill/France93.shp') %>% st_transform(  2154) 
# lim2 <- as(lim, 'Spatial')
# lim3 <- lim2[1,]
# plot(lim3)
# lim3@polygons[[1]]@Polygons[[1]]@coords <- lim3@polygons[[1]]@Polygons[[1]]@coords / 100000
# 
# 
# sapply(slot(lim2, 'polygons'), function(i) slot(i, 'area')) 
# 
# xyMesh=as.matrix(Data_zone[,c("x","y")])##build coordinate matrix
# xyMesh<-xyMesh/100000
# 
# max.edge = 0.3
# bound.outer = 3 #0.7
# mesh = inla.mesh.2d(boundary = lim3,
#                     loc=xyMesh,
#                     max.edge = c(1,8)*max.edge, # au lieu de 5
#                     cutoff = 0.1,
#                     offset = c(max.edge, bound.outer),
#                     min.angle=21)
# plot(mesh)
# points( unique(xyMesh) ,col=2,pch=4)

save(Data_zone,file = 'data/DataFRAAcidMatParPEquivOlsen.RData')

