# 1 Packages-------------------------------------------------------
library(INLA)
library(sf)
library(RODBC)
library(dplyr)
library(tidyverse)
library(xlsx)
library(foreign)
library(sp)
library(stringr)
library(raster)


# 2 collect data-------------------------------------------------------
# coordinates of the center of the communes
# coord <- read.table('C:/Users/nsaby/SharePoint/Pari Scientifique - Documents 1/data/bdat/BDAT_xy.csv',
#                     header=TRUE,sep=';',dec='.' )

coord <- read.table("D:/unsavensabyD/Pari/7/Pari Scientifique - Do~1/data/bdat/BDAT_xy.csv",
                    header=TRUE,sep=';',dec='.' )
# Collect BDAT

connection  <- odbcConnect("BDAT")

RequeteBDAT <- "
SELECT
bdatid,
annee,
mois,
insee,
pho,
k2o ,
X_commune as x, Y_COMMUNE as Y
FROM
data.bdat_com
where annee > 1990 and annee < 2021
and depart in ('29','56','35','22')
"
DataBDAT <- sqlQuery(connection, 
                     RequeteBDAT,
                     as.is = FALSE)

odbcCloseAll()

DataBDAT$bdatid <- 1:nrow(DataBDAT)
DataBDAT$insee <- factor(str_pad(DataBDAT$insee,5,sid='left','0'))



DataBDAT2 <- DataBDAT %>%
  mutate(k2o = ifelse( k2o < 0 ,
                       k2o * -0.5 , 
                       k2o )) %>% # under DL
  dplyr::filter(  !is.na(insee)  ) %>% # eliminate a lot of data
  dplyr::filter( (!is.na(k2o) & k2o <1200  ) ) %>%
  filter(!is.na(x))
  

summary(DataBDAT2)

table(DataBDAT2$annee)
summary(DataBDAT2)
hist(DataBDAT2$k2o,breaks = 150)

# Collect the communes limits
 communes <- read_sf('D:/unsavensabyD/Pari/7/Pari Scientifique - Do~1/data/admin/COMMUNE.shp') %>% st_set_crs(  2154) 
 communes <- communes %>% filter(CODE_DEPT %in% c("29","35","22","56"))

# 3 Prepare ph data  for modelling-------------------------------------------------------

# 3.1 add insee name and coordinates
# resu1 <- coord %>% 
#   st_as_sf(coords = c("X_L93", "Y_L93"), 
#            crs = 2154, agr = "constant") %>%
#   st_join(communes) %>%
#   dplyr::select(annee,insee,INSEE_COM) %>% 
#   mutate(x = unlist(purrr::map(.$geometry,1)),
#          y = unlist(purrr::map(.$geometry,2))) %>%
#   st_set_geometry(NULL)
# 
# DataBDAT2 <- resu1 %>%
#   right_join(DataBDAT2,by = c('insee'='insee','annee'='annee')) %>%
#   filter(!is.na(INSEE_COM)) %>% distinct()
# 
# id <- table(DataBDAT2$bdatid)[which(table(DataBDAT2$bdatid)>1)]
# 
# DataBDAT2[DataBDAT2$bdatid %in% as.numeric(names(id)),]

 resu1 <- DataBDAT2 %>% 
     st_as_sf(coords = c("x", "y"),
              crs = 2154, agr = "constant") %>%
     st_join(communes) %>%
     dplyr::select(bdatid,INSEE_COM) %>%
     st_set_geometry(NULL)
 
 DataBDAT2 <- DataBDAT2 %>%
   right_join(resu1 ,
              by = c('bdatid')) %>%
   filter(!is.na(INSEE_COM)) 

##Extract code for department
DataBDAT2$depart <- substr(DataBDAT2$INSEE_COM,1,2)

as.data.frame(table(DataBDAT2$depart))


## delete islands

DataBDAT2 <- DataBDAT2 %>%
  filter(
    !insee %in% c("56241","56009","56114","56186","56234","56086",
                  "56152","56082","56085","56069","29155")
  )

as.data.frame(table(DataBDAT2$depart))

# 4 Compute fixed effects --------------------------------------------------------
# Number of data ---------
NbAna <- DataBDAT2 %>% 
  group_by(INSEE_COM) %>%
  summarise(n = n()) %>%
  mutate(n2 = n /max(n),# to normalized the number of data per commune
         nlog= log(n)) # we prefer to keep  n , then all values are positive

DataBDAT2 <- DataBDAT2 %>%
  left_join(NbAna,by = c('INSEE_COM'='INSEE_COM'))

hist(NbAna$n,breaks = 150)
boxplot(NbAna$n,breaks = 150)

N = 2500 # for selecting commune with a lot of data

id <- which(NbAna$n>N)

DataBDAT2 %>%
  filter(INSEE_COM %in% NbAna$INSEE_COM[id]) %>%
  ggplot(aes(INSEE_COM,k2o) ) +
  geom_boxplot() +
  coord_flip()

# Parental Material ----
mat <- raster('D:/unsavensabyD/Pari/7/Pari Scientifique - Do~1/data/MatparBret/pred_sub_bzh.tif')


library(exactextractr)
res <- exact_extract(mat, communes, 'majority')

communes$matcl = res

# library(mapview)
# mapview(communes, zcol='matBret') + mapview(mat)


DataBDAT2 <- DataBDAT2 %>%
  left_join(  communes[, c("INSEE_COM","matcl")] %>% st_set_geometry( NULL) ,
            by = c('insee'='INSEE_COM'))


# Seamzones / Climate --------
library(lwgeom)

climate <- read_sf('R:/Landmark/WP/WP2/Data/JRC/Seamzones/shapes_Liliane/seamzones_environmentalzone.shp') %>%
  st_transform( crs = 2154) %>% st_make_valid()

# Too long : climate <-  climate %>% st_intersection(communes)

climateJ <-  st_join(
  st_as_sf(dplyr::select(DataBDAT2,bdatid,x,y) ,
                   coords = c("x", "y"), crs = 2154, agr = "constant"),
  climate
  )
  
DataBDAT2 <- DataBDAT2 %>%
  left_join(dplyr::select(climateJ,bdatid,FIRST_ENZ) ,by = c('bdatid'='bdatid'))

DataBDAT2$FIRST_ENZ <- factor(DataBDAT2$FIRST_ENZ)

DataBDAT2$nlogCl <- cut(DataBDAT2$nlog,
                        breaks = quantile(DataBDAT2$nlog,
                                          prob = c(0,.25,.5,.75,1) ), 
                        include.lowest = TRUE  )

DataBDAT2$k2o= DataBDAT2$k2o
saveRDS(DataBDAT2,file='data/DataBretAcidMatPark2oRaw.RDS') 

# Perform a selection of a subset of data -----------


Data_zone<-DataBDAT2 %>%
  filter(
    ! depart %in%  c('2A','2B') &
      !is.na(matcl) &
      !is.na(FIRST_ENZ) &
      !is.na(nlogCl) &
      n<N &
      !is.na(k2o) 
      # & annee > 1994
  ) %>%
  group_by(x,y,annee,matcl,FIRST_ENZ,nlogCl) %>% 
  summarise(k2o= mean(k2o),
            
            n=n()) %>% 
  ungroup() %>%
  as.data.frame()

  # sample_frac(.4) %>%
  

ggplot(Data_zone[,],aes(as.factor(matcl) , k2o) ) + geom_boxplot() 
ggplot(Data_zone[,],aes(FIRST_ENZ , k2o) ) + geom_boxplot() 
ggplot(Data_zone[,],aes(nlogCl , k2o) ) + geom_boxplot() 
hist(Data_zone$k2o,breaks=30)
hist(log( Data_zone$k2o),
     breaks=30)
hist(sqrt( Data_zone$k2o),
     breaks=30)

# Data_zone %>%
#   left_join(NbAna,by = c('INSEE_COM'='INSEE_COM')) %>%
#   dplyr::filter(INSEE_COM %in% NbAna$INSEE_COM[id]) %>%
#   ggplot(aes(INSEE_COM,k2o) ) + 
#   geom_boxplot() +
#   coord_flip()

## Implement the cross validation ----------

# ST cross validation
set.seed(123098)

size = Data_zone %>% distinct(x,y) %>% dim 

xy <- Data_zone %>% distinct(x,y) %>%
  mutate(fold = sample(1:5,size=size[1], replace = TRUE))

ggplot(xy[1:50,],aes(x,y,col=factor(fold)  ))  + geom_point()

Data_zone <- Data_zone %>% left_join(xy, by=c('x','y'))

# Classical Xvalication
Data_zone <- Data_zone %>% 
  mutate(fold2 = sample(1:5, size=dim(Data_zone)[1], replace = TRUE))

# size = Data_zone %>% distinct(x,y,annee) %>% dim 
# xya <- Data_zone %>% 
#   distinct(x,y,annee) %>%
#   mutate(fold3 = sample(1:5,
#                         size=size[1],
#                         replace = TRUE)
#          )
# 
# ggplot(xya %>% filter(x==134700   & y==6853800),aes(x,y,col=factor(fold3)  ))  +
#   geom_jitter(width = 5, height = 5)
# 
# xya %>% filter(x==134700   & y==6853800) %>% arrange(annee)
# 
# xya %>% filter(annee==2001 & x==134700   & y==6853800)
# 
# Data_zone %>% group_by(x,y,annee) %>% summarise(n = n())%>% filter(n>1)
# 
# Data_zone <- Data_zone %>% left_join(xya, by=c('x','y','annee'))


# Prepare mesh
# lim <- read_sf('D:/database/habill/France93.shp') %>% st_transform(  2154) 
# lim2 <- as(lim, 'Spatial')
# lim3 <- lim2[1,]
# plot(lim3)
# lim3@polygons[[1]]@Polygons[[1]]@coords <- lim3@polygons[[1]]@Polygons[[1]]@coords / 100000
# 
# 
# sapply(slot(lim2, 'polygons'), function(i) slot(i, 'area')) 
# 
# xyMesh=as.matrix(Data_zone[,c("x","y")])##build coordinate matrix
# xyMesh<-xyMesh/100000
# 
# max.edge = 0.3
# bound.outer = 3 #0.7
# mesh = inla.mesh.2d(boundary = lim3,
#                     loc=xyMesh,
#                     max.edge = c(1,8)*max.edge, # au lieu de 5
#                     cutoff = 0.1,
#                     offset = c(max.edge, bound.outer),
#                     min.angle=21)
# plot(mesh)
# points( unique(xyMesh) ,col=2,pch=4)

save(Data_zone,file = 'data/DataBretAcidMatPark2o.RData')

