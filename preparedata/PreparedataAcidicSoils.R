# 1 Packages-------------------------------------------------------
library(INLA)
library(sf)
library(RODBC)
library(dplyr)
library(tidyverse)
library(xlsx)
library(foreign)
library(sp)
library(stringr)

setwd("D:/unsavensabyD/Pari/ph/script")

# 2 collect data-------------------------------------------------------
# coordinates of the center of the communes
coord <- read.table('C:/Users/nsaby/SharePoint/Pari Scientifique - Documents 1/data/bdat/BDAT_xy.csv',
                    header=TRUE,sep=';',dec='.' )
# Collect BDAT

connection  <- odbcConnect("BDATdonesol364")

RequeteBDAT <- "
SELECT
bdatid,
annee,
mois,
insee,
pho,
CASE WHEN pho < 7.1 AND catot is null THEN 0 ELSE catot END as catot

FROM
data.bdat_com
where annee > 1990 and annee < 2015
and pho > 4 and pho < 12  
and depart not in ('75','92','93','2A','2B')
"
DataBDAT <- sqlQuery(connection, RequeteBDAT,as.is = FALSE)

odbcCloseAll()

DataBDAT$bdatid <- 1:nrow(DataBDAT)
DataBDAT$insee <- factor(str_pad(DataBDAT$insee,5,sid='left','0'))

# extract ph of non calcareous soil with month

tt <- cbind(DataBDAT$pho,!is.na(DataBDAT$catot) & DataBDAT$catot < 2.2,
      is.na(DataBDAT$catot)  & DataBDAT$pho< 7.5,
      DataBDAT$annee<1995)

table(tt[,4],tt[,3])
table(tt[,4],tt[,2])

DataBDAT2 <- DataBDAT %>%
  dplyr::filter( !is.na(mois) & !is.na(insee) & annee > 1995 ) %>% # eliminate a lot of data
  dplyr::filter( (!is.na(catot) & catot < 2.2)  |
                 ( is.na(catot)  & pho< 7.5) ) %>%
  dplyr::filter(pho < 9)
  


table(DataBDAT2$annee)
summary(DataBDAT2)
hist(DataBDAT2$pho,breaks = 150)
# Collect the communes limits
communes <- read_sf('C:/Users/nsaby/SharePoint/Pari Scientifique - Documents 1/data/admin/COMMUNE.shp') %>% st_set_crs(  2154) 

# 3 Prepare ph data  for modelling-------------------------------------------------------

# 3.1 add insee name and coordinates
resu1 <- coord %>% 
  st_as_sf(coords = c("X_L93", "Y_L93"), 
           crs = 2154, agr = "constant") %>%
  st_join(communes) %>%
  select(annee,insee,INSEE_COM) %>% 
  mutate(x = unlist(purrr::map(.$geometry,1)),
         y = unlist(purrr::map(.$geometry,2))) %>%
  st_set_geometry(NULL)

DataBDAT2 <- resu1 %>%
  right_join(DataBDAT2,by = c('insee'='insee','annee'='annee')) %>%
  filter(!is.na(INSEE_COM)) %>% distinct()

id <- table(DataBDAT2$bdatid)[which(table(DataBDAT2$bdatid)>1)]

DataBDAT2[DataBDAT2$bdatid %in% as.numeric(names(id)),]
##Extract code for department
DataBDAT2$depart <- substr(DataBDAT2$INSEE_COM,1,2)

as.data.frame(table(DataBDAT2$depart))

# 4 Compute fixed effects --------------------------------------------------------
# Number of data ---------
NbAna <- DataBDAT2 %>% 
  group_by(INSEE_COM) %>%
  summarise(n = n()) %>%
  mutate(n2 = n /max(n),# to normalized the number of data per commune
         nlog= log(n)) # we prefer to keep  n , then all values are positive

DataBDAT2 <- DataBDAT2 %>%
  left_join(NbAna,by = c('INSEE_COM'='INSEE_COM'))


N = 800 # for selecting commune with a lot of data

id <- which(NbAna$n>N)

DataBDAT2 %>%
  filter(INSEE_COM %in% NbAna$INSEE_COM[id]) %>%
  ggplot(aes(INSEE_COM,pho) ) +
  geom_boxplot() +
  coord_flip()

# Parental Material ----
#
smucom <- read.csv2('C:/Users/nsaby/SharePoint/Pari Scientifique - Documents 1/data/bdat/com2012-SMU-allfrance.csv') %>%
  rename(insee = ins_cmm.C.80 ,  SMU = SMU.N.10.0 , AREA  = AREA.N.19.11,
         PERCENTAGE = PERCENTAGE.N.19.11) %>%
  group_by(insee,SMU) %>%
  summarise(pcSMU = max(PERCENTAGE)) %>%
  ungroup()

library(foreign)
stu <- read.dbf('D:/database/pedo/france/stu.dbf')
stu.org <- read.dbf('D:/database/pedo/france/stuorg.dbf')

# Reclassified the parental material in 8 cl
stu$matcl <- as.character(stu$MAT11)
stu$matcl[stu$MAT12 %in% c('33','35')] <- '2' #Calcareous clay, Residual clay from calcareous rocks
stu$matcl <- factor(stu$matcl)

smuMAT <- stu.org %>% 
  left_join( select(stu,STU,matcl),
             by = 'STU') %>%  
  # left_join(mat,by =c('MAT1' ='mat') ) %>% 
  
  select(SMU,STU,PCAREA,matcl) %>% na.omit() %>%
  select(-STU) %>% group_by(SMU , matcl) %>%
  summarise_each(  funs(sum) , PCAREA ) %>% 
  mutate(the_rank  = rank(-PCAREA, ties.method = "random")) %>%
  filter(the_rank == 1) %>% select(-the_rank) %>%
  as.data.frame()

comMAt <- smucom %>% 
  left_join(smuMAT, by='SMU') %>%
  mutate(pcfinal = (pcSMU * (PCAREA / 100) ) )%>%
  select(insee,pcfinal,matcl) %>% na.omit()  %>% 
  group_by(insee , matcl) %>%
  summarise_each(  funs(sum) , pcfinal ) %>% 
  mutate(the_rank  = rank(-pcfinal, ties.method = "random")) %>%
  filter(the_rank == 1) %>% select(-the_rank) %>%
  as.data.frame() 



DataBDAT2 <- DataBDAT2 %>%
  left_join(comMAt,by = c('insee'='insee'))


# Seamzones / Climate --------
library(lwgeom)
climate <- read_sf('O:/WP/WP2/Data/JRC/Seamzones/shapes_Liliane/seamzones_environmentalzone.shp') %>%
  st_transform( crs = 2154) %>% st_make_valid()

# Too long : climate <-  climate %>% st_intersection(communes)

climateJ <-  st_join(
  st_as_sf(dplyr::select(DataBDAT2,bdatid,x,y) ,
                   coords = c("x", "y"), crs = 2154, agr = "constant"),
  climate
  )
  
DataBDAT2 <- DataBDAT2 %>%
  left_join(dplyr::select(climateJ,bdatid,FIRST_ENZ) ,by = c('bdatid'='bdatid'))

DataBDAT2$FIRST_ENZ <- factor(DataBDAT2$FIRST_ENZ)

DataBDAT2$nlogCl <- cut(DataBDAT2$nlog,
                        breaks = quantile(DataBDAT2$nlog,
                                          prob = c(0,.25,.5,.75,1) ), 
                        include.lowest = TRUE  )

saveRDS(DataBDAT2,file='D:/unsavensabyD/Pari/ph/dataBDATAcidicSoils.RDS') 

# Perform a selection of a subset of data -----------


Data_zone<-DataBDAT2 %>%
  filter(
    ! depart %in%  c('2A','2B') &
      !is.na(matcl) &
      !is.na(FIRST_ENZ) &
      !is.na(nlogCl) &
      n<N &
      !is.na(pho) 
      # & annee > 1994
  ) %>%
  group_by(x,y,annee,mois,matcl,FIRST_ENZ,nlogCl) %>% 
  summarise(ph= -log( mean(  exp(-pho) ) ),
            phExp=  mean(  exp(-pho)  ),
            n=n()) %>% 
  ungroup() %>%
  as.data.frame()

  # sample_frac(.4) %>%
  

ggplot(Data_zone[,],aes(matcl , ph) ) + geom_boxplot() 
ggplot(Data_zone[,],aes(FIRST_ENZ , ph) ) + geom_boxplot() 
ggplot(Data_zone[,],aes(nlogCl , ph) ) + geom_boxplot() 
hist(Data_zone$ph,breaks=30)

Data_zone %>%
  filter(INSEE_COM %in% NbAna$INSEE_COM[id]) %>%
  ggplot(aes(INSEE_COM,pho) ) + geom_boxplot() +coord_flip()

## Implement the cross validation ----------

# ST cross validation
set.seed(123098)

size = Data_zone %>% distinct(x,y) %>% dim 

xy <- Data_zone %>% distinct(x,y) %>%
  mutate(fold = sample(1:5,size=size[1], replace = TRUE))

Data_zone <- Data_zone %>% left_join(xy, by=c('x','y'))

# Classical Xvalication
Data_zone <- Data_zone %>% 
  mutate(fold2 = sample(1:5, size=dim(Data_zone)[1], replace = TRUE))

# Prepare mesh
lim <- read_sf('D:/database/habill/France93.shp') %>% st_transform(  2154) 
lim2 <- as(lim, 'Spatial')
lim3 <- lim2[1,]
plot(lim3)
lim3@polygons[[1]]@Polygons[[1]]@coords <- lim3@polygons[[1]]@Polygons[[1]]@coords / 100000


sapply(slot(lim2, 'polygons'), function(i) slot(i, 'area')) 

xyMesh=as.matrix(Data_zone[,c("x","y")])##build coordinate matrix
xyMesh<-xyMesh/100000

max.edge = 0.3
bound.outer = 3 #0.7
mesh = inla.mesh.2d(boundary = lim3,
                    loc=xyMesh,
                    max.edge = c(1,8)*max.edge, # au lieu de 5
                    cutoff = 0.1,
                    offset = c(max.edge, bound.outer),
                    min.angle=21)
plot(mesh)
points( unique(xyMesh) ,col=2,pch=4)

save(Data_zone,lim3,file = 'C:/Users/nsaby/SharePoint/Pari Scientifique - Documents 1/data/bdat/DataFrancephAcidicSoilsv1.RData')
