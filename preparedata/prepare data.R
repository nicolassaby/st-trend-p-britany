setwd("D:/unsavensabyD/Pari/7/Pari Scientifique - Do~1/")

library(dplyr)
library(tidyr)
library(sf)
library(stringr)


load('data/bdat/DataFrancePdyer.RData')

# Data_zone <- readRDS('results/ST model of Soil P in Bretagne/Harmonized P/Data/dataBDATPequivO.RDS')

lim4 <- readRDS('results/ST model of Soil P in Bretagne/Harmonized P/Data/limitBritany.RDS')

plot(lim4)


# Extract data C from Data_zone

xy <- Data_zone %>% 
  mutate(id=1:dim(Data_zone)[1] ) %>% 
  select(id ,x,y)

canton <- read_sf('results/ST model of Soil P in Bretagne/Harmonized P/Data/canton.shp') %>%
  st_transform(2154)


join <- xy %>%
  st_as_sf( coords = c("x", "y"), crs = 2154, agr = "constant") %>%
  st_join(canton) %>%
  mutate(dep = str_sub(dcan,1,2)) %>%
  dplyr::filter(dep %in% c('22','29','35','56'))
  
Data_zone <- Data_zone[ join$id, ] %>%
  filter(n<1100)

Data_zone %>% 
  dplyr::filter(INSEE_COM=="22316") %>%
  ggplot(aes(factor(annee),pdyer))+geom_boxplot()

# test 
library(INLA)

xyMesh=as.matrix(Data_zone[,c("x","y")])##build coordinate matrix
xyMesh<-xyMesh/100000

max.edge = 0.3
bound.outer = 3 #0.7
mesh = inla.mesh.2d(boundary = lim4,
                    loc=xyMesh,
                    max.edge = c(1,8)*max.edge, # au lieu de 5
                    cutoff = 0.1,
                    offset = c(max.edge, bound.outer),
                    min.angle=21)
plot(mesh)
points( unique(xyMesh) ,col=2,pch=4)

lim3 = lim4

save(Data_zone,lim3,file = 'data/bdat/DataBretagnePdyer.RData')
